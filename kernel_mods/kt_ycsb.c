// A set of Cassandra specific functions to parse its header etc.
#include <linux/kernel.h>
#include "ktracer.h"

#include <linux/inet.h> // in_aton
#include <linux/inetdevice.h>

/** Basically the problem is the padding in the struct ycsb_msg_payload, thus we cannot
simply align bytes in the payload with the structure which has been padded by compiler**/
inline uint8_t ycsb_get_version_from_payload(uint8_t * tcp_payload){
    return *(tcp_payload); // see struct ycsb_msg_payload for explanation
}
inline uint16_t ycsb_get_streamid_from_payload(uint8_t * tcp_payload){
    return *(uint16_t *)(tcp_payload + 2); // see struct ycsb_msg_payload for explanation
}
inline uint8_t ycsb_get_opcode_from_payload(uint8_t * tcp_payload){
    return *(tcp_payload + 4); // see struct ycsb_msg_payload for explanation
}

inline uint32_t ycsb_get_order_id4incoming_packet(uint8_t opcode){

    if (opcode == YCSB_OPCODE_QUERY) {
        return 3;
    } else if (opcode == YCSB_OPCODE_RESULT) {
        return 7;
    }
    printk(KERN_ERR "ERROR Unknown incoming YCSB opcode [%u][%u][%i]!\n", opcode, ntohl(opcode), sizeof(struct ycsb_msg_payload));
    return 0;
}

inline uint32_t ycsb_get_order_id4outgoing_packet(uint8_t opcode){

    if (opcode == YCSB_OPCODE_QUERY) {
        return 2;
    } else if (opcode == YCSB_OPCODE_RESULT) {
        return 6;
    }
    printk(KERN_ERR "ERROR Unknown outgoing YCSB opcode [%u]!\n", opcode);
    return 0;
}

// Check if we are prepare to parse this type of messages
inline int ycsb_is_known_operation(uint8_t opcode) {
    return ((opcode == YCSB_OPCODE_QUERY) || (opcode == YCSB_OPCODE_RESULT));
}
