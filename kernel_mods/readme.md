# Tectonic Kernel Modulus

There are modulus **Filter** and **Tracer**.

## Configuration

Common use of Tectonic requires it to be present on both end hosts i.e., on the sender's and receiver's sides.
Moreover, it needs to know all the IP addresses of the cluster and sometime an additional information that is required for a given tasks (e.g., flow IPs, next hop MAC addresses).
The code is optimized in the way to reduce the number of branching points and redundancies, thus a lot of #ifdef #else statements is used.
To minimize manual code modification for each particular scenario, two approaches are used:

**First** level configuration comes from the **Makefile**.
There, user can enable various logging, statistics, or disable functionalities, prior to building kernel modulus.
This is a high level configuration.

The **second** level configuration is done via **auto_config_tectonic.py** + **config_tec.json**.
The latter configuration file describes the set of machines among which Tectonic is deployed (e.g., all IP and MAC addresses) and the type of experiment/measurement that tectonic is designed to do.
Based on the content of this configuration file **auto_config_tectonic.py** script generates/modifies specific C source files of Tectonic prior to compilation.

Generally, the **config_tec.json** should be consistent among _all_ end hosts where Tectonic runs.
_However_,  one distinct property is the each host's index or ''this_node_order'' as defined in **auto_config_tectonic.py**.
This is a unique number that allows each instance of Tectonic to identify itself in the global list of IPs, in other words this is what determines the order at which each instance acts.
This parameter can be supplied via args to the **auto_config_tectonic.py** or read directly from the configuration file.

### Config_Tec.JSON

	The **config_tec.json** can be configured manually and then distributed among all hosts.
	Alternatively the **az.py** script has a module that perform's this task automatically and distributes configurations among the EC2 instances.
	Currently, manual configuration is required if Tectonic is running on the heterogeneous cluster, e.g., an additional instance in nslrack (or elsewhere) needs to be included.
	See az.py readme for the detailed explanation of the **config_tec.json**


## Compilation

- ''auto_config_tectonic.py''
- ''make''
- ''make clean''


## Run

- ''sudo rmmod ktracer''
- ''sudo rmmod kfltr''
- ''sudo insmod ./kfltr.ko''
- ''sudo insmod ./ktracer.ko''

OR

- ''make clean; make ; sudo rmmod ktracer; sudo rmmod kfltr ; sudo insmod ./kfltr.ko; sleep 1 ; sudo insmod ./ktracer.ko''


## Output /PROC file system

The following is the list of /proc files through which Tectonic reports various measurements.

- **/proc/ktin.log** for Scylla/Cassandra reports events of incoming requests.
- **/proc/ktout.log** for Scylla/Cassandra reports events of outgoing responses.
- **/proc/ktracer.bounce.log** reports reception of a probe from a remote instance of tectonic. Such probes can be part of the traceroute or latency measurements and often being "bounced" back to the original sender with timestamps and some updated meta data.
- **/proc/ktracer.loadfeedback.log** for Scylla and Cassandra reports service times on the remote server.
- **/proc/ktracer.receiver.log** reports the event of reception of bounced probes from remote nodes. Note, all probes send from remote nodes towards us are logged in the bounced probe log file.
- **/proc/ktracer.sender.log** reports all probes that are being sent from this (i.e., local) instance of tectonic towards some remote tectonic instances.


- **Kernel Log** use ''tail -f /var/log/kern.log'' to read debug messages from Tectonic
.. - ''alias ttail='tail -f /var/log/kern.log | cut -c 55-' ''