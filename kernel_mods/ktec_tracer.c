#include <linux/tcp.h>
#include <linux/ip.h>
#include <net/tcp.h>
#include <net/checksum.h>
#include <linux/icmp.h>

// #include "ktproc.h"

#include <linux/sort.h>
#include <linux/inet.h>

#include "ktracer.h"


// extern struct trc_probe_payload;


void debug_print(char * out_buf, char * buf2print, int len);
#define IP_STR_LEN 16




void __assemble_fltr2usr_resp_report(struct trc_bounce_usr_report * uspace_msg,
    uint32_t tcp_seq, uint32_t src_ip, uint32_t dst_ip,
    uint64_t bounce_time_ns, uint64_t rcv_time_ns, uint64_t rtt_ns) {

    uspace_msg->type = TRC_BOUNCED_PROBE;
    uspace_msg->tcp_seq = tcp_seq;
    uspace_msg->src_ip = src_ip;
    uspace_msg->dst_ip = dst_ip;
    uspace_msg->bounce_time_ns = bounce_time_ns;
    uspace_msg->rcv_time_ns = rcv_time_ns;
    uspace_msg->rtt_ns = rtt_ns;
}


inline void assemble_fltr2usr_resp_report(struct trc_bounce_usr_report * uspace_msg, struct sk_buff *skb) {

    static struct iphdr *iph;
    static struct tcphdr * tcph;
    static struct trc_probe_payload * trc_probe_payload;
    static uint64_t time_now_ns;
    time_now_ns = ktime_get_real().tv64;

    iph = (struct iphdr *)skb_network_header(skb);
    tcph = (struct tcphdr *)skb_transport_header(skb);


    trc_probe_payload = (struct trc_probe_payload *)((unsigned char *)tcph + (tcph->doff * 4));

    __assemble_fltr2usr_resp_report(uspace_msg, tcph->seq, ntohl(iph->daddr), ntohl(iph->saddr),
        trc_probe_payload->bounce_time_ns,
        time_now_ns,
        time_now_ns - trc_probe_payload->send_time_ns);

}

inline void assemble_fltr2usr_icmp_report(struct trc_icmpte_usr_report * uspace_msg, struct sk_buff *skb) {

    static struct iphdr *iph;
    static struct iphdr *orig_iph;

    static struct icmphdr * icmph;
    static uint32_t icmp_tcp_seq_offset;

    iph = (struct iphdr *)skb_network_header(skb);
    icmph = (struct icmphdr *)icmp_hdr(skb);

    orig_iph = (struct iphdr *)((unsigned char*)icmph + sizeof(struct icmphdr));

    icmp_tcp_seq_offset = sizeof(struct icmphdr) +
                          sizeof(struct  iphdr) +
                          4; // Skipping 2+2 bytes src/dst ports of IP



    uspace_msg->type = TRC_ICMPTE_PROBE;
    uspace_msg->tcp_seq = *(uint32_t*)((unsigned char*)icmph+icmp_tcp_seq_offset);

    uspace_msg->hop_ip = ntohl(iph->saddr);

    uspace_msg->src_ip = ntohl(orig_iph->saddr);
    uspace_msg->dst_ip = ntohl(orig_iph->daddr);
    uspace_msg->rcv_time_ns = ktime_get_real().tv64;

}


