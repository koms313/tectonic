/* See has examples: https://danielmaker.github.io/blog/linux/list_hlist_hashtable.html */
#include <linux/hashtable.h>
#include <linux/kernel.h>
#include <linux/slab.h>      // for kmalloc() dynamic allocation
#include <linux/kfifo.h>
#include <linux/mutex.h>

#include "ktracer.h"

/* This is the maximum size of the containers spread among all hash tables */
#define MATCHER_MAX_CONTAINERS 1000

/* When the number of remaining containers will fall below this threshold GC will attempt
to remove old entries */
#define MATCHER_GARBAGE_COLLECT_FREE_CON_THRESHOLD 0.1

/* During GC requests that stay in the has table over that period of time will be removed*/
#define MATCHER_GARBAGE_COLLECT_REQUEST_AGE_THRESHOLD_NS 1*1000*1000*1000

/* The number of buckets per hash table 2^X */
#define MATCHER_BUCKETS_PER_HASH_TABLE 9

DEFINE_HASHTABLE(htable, MATCHER_BUCKETS_PER_HASH_TABLE);

// setting up FIFO_SIZE macro based on the sizeof(matched_request_responce_feedback)
// gives compilation errors, thus, preallocate size manually keeping in mind the size
// of the matched_request_responce_feedback structure. The size should be 2^X
#define FIFO_SIZE  1024
typedef STRUCT_KFIFO_REC_1(FIFO_SIZE) fifo_queue;
static fifo_queue fifo_load_feedback_queue;

static DEFINE_MUTEX(matched_packets_gc_lock); // Garbage collect lock

static inline bool mutex_try_to_acquire(struct mutex *lock) {
    return !mutex_is_locked(lock) &&
        (atomic_cmpxchg_acquire(&lock->count, 1, 0) == 1);
}

int send_udp_message2same_destination(struct sk_buff *target_skb, struct net_device * dev,
    uint32_t dst_port, unsigned char * usr_data, uint16_t usr_data_len);

struct request_record_container {

    uint32_t request_id;
    uint64_t tstamp_ns;
    uint32_t src_ip;

    /* All free containers organized into a linked list, thus we can quickly pick
       up a next available container without allocating/deallocating ram at run-time */
    struct request_record_container * ptr_next_free_cont;

    /* Meta data indicates the number of outstanding free containers, used for the
    debugging purposes. If we look at the  global_ptr_free_con and check this variable
    we will know how many free containers remained to allocate*/
    uint32_t meta_remaining_size;

    // This structure is needed for the container to be 'hashable'
    struct hlist_node node;
};

/* This is a single global pointer which points to the next free container that can be
used, it is never NULL at run-time. */
struct request_record_container * ptr_free_con;


void allocate_free_request_record_containers(int n){
    int i;
    struct request_record_container * new_obj;

    ptr_free_con = kmalloc(sizeof(struct request_record_container), GFP_KERNEL);

    ptr_free_con->ptr_next_free_cont = NULL;
    ptr_free_con->meta_remaining_size = 0;

    // loop [0..n] + initial free_request_record_container, total number of elements is N+1
    for (i = 0; i < n; i++){
         new_obj = kmalloc(sizeof(struct request_record_container), GFP_KERNEL);

        new_obj->ptr_next_free_cont = ptr_free_con;
        new_obj->meta_remaining_size = i; // for debugging purposes

        ptr_free_con = new_obj;
    }
}

void deallocate_request_record_containers(void){
    /*Note: function subject to hash table(s) being freed up*/
    struct request_record_container * next = ptr_free_con;

    while (ptr_free_con != NULL) {

        next = ptr_free_con->ptr_next_free_cont;

        kfree(ptr_free_con);

        ptr_free_con = next;
    }

}

struct request_record_container * get_free_request_record_container(void){
    static struct request_record_container * ret;

    if (ptr_free_con->ptr_next_free_cont == NULL) {
        /* We do not return the very last request_record_container in the linked list */
        return NULL;
    } else {
        ret = ptr_free_con;

        ptr_free_con = ptr_free_con->ptr_next_free_cont;
    }
    return ret;

}

inline void reclaim_request_record_container(struct request_record_container * obj){
    /* Since we never return the very last ptr_free_con, we guaranteed that it is not NULL
    This we can proceed without an if statement here */
    obj->ptr_next_free_cont = ptr_free_con;
    obj->meta_remaining_size = ptr_free_con->meta_remaining_size + 1;

    ptr_free_con = obj;
}


void matcher_init_hash_tables(void){
    printk(KERN_INFO "matcher_init_hash_tables\n");
    allocate_free_request_record_containers(MATCHER_MAX_CONTAINERS);
}

void matcher_free_hash_tables(void){
    printk(KERN_INFO "matcher_free_hash_tables\n");
    deallocate_request_record_containers();
}

void matcher_init_FIFO_queue(void) {
    INIT_KFIFO(fifo_load_feedback_queue);
}

void matcher_free_FIFO_queue(void) {
    kfifo_free(&fifo_load_feedback_queue);
}

void matcher_add_request2hash(uint32_t request_id, uint64_t tstamp_ns, uint32_t src_ip) {
    static struct request_record_container * con;
    static uint32_t key;
    con = get_free_request_record_container();

    if (con == NULL) {
        printk(KERN_INFO "No more free containers to preserve incoming requests\n");
        return;
    }

    // <1.> set correct data into container
    con->request_id = request_id;
    con->tstamp_ns = tstamp_ns;
    con->src_ip = src_ip;

    // <2.> add container into the hash table
    key = request_id + src_ip;
    hash_add(htable, &con->node, key);

    // printk(KERN_INFO MATCHER"Remaining number of conts: [%i][%lld]\n", ptr_free_con->meta_remaining_size, tstamp_ns);

}



int matcher_match_response_from_hash(uint32_t request_id, uint32_t src_ip) {

    static struct request_record_container * iobj;
    static uint32_t key;

    static struct matched_request_responce_feedback mrrf;
    static int ret;

    key = request_id + src_ip;

    if (mutex_try_to_acquire(&matched_packets_gc_lock)) {

        hash_for_each_possible(htable, iobj, node, key) {
            if ( (iobj->request_id == request_id) && (iobj->src_ip == src_ip)) {
                // We have an exact match!
                // printk(KERN_INFO MATCHER"MATCH\t key=%u => %u %lld %u delta [%lld]\n", key,
                // iobj->request_id, iobj->tstamp_ns, iobj->src_ip, ktime_get_real().tv64-iobj->tstamp_ns);


                mrrf.lfp.total_service_time_ns = ktime_get_real().tv64-iobj->tstamp_ns;
                printk(KERN_INFO MATCHER"MATCH\t lfp %lld\n", mrrf.lfp.total_service_time_ns);

                // Here we need to remove this entry from the hash table.
                hash_del(&iobj->node);

                reclaim_request_record_container(iobj);

                ret = kfifo_in(&fifo_load_feedback_queue,
                               &mrrf,
                               sizeof(struct matched_request_responce_feedback) );

                // printk(KERN_INFO FLTR"Inserting into the fifo\n");
                if (ret == 0) {
                   printk(KERN_INFO MATCHER"WARNING fifo_load_feedback_queue is full!\n");
                }

                mutex_unlock(&matched_packets_gc_lock);
                return 1;
            }
        }

    mutex_unlock(&matched_packets_gc_lock);
    }

    return 0;

}

void matcher_garbage_collect_hash_table(void) {
    /* Remove old entries in the Hash Table if needed */
    static struct request_record_container * iobj;
    static int key = 2;
    static int a;
    static struct hlist_node * tmp;
    static uint64_t time_now_ns;
    static int gc_count;
    gc_count = 0;

    // is it time to GC?
    if (ptr_free_con->meta_remaining_size <
        (int)(MATCHER_MAX_CONTAINERS * MATCHER_GARBAGE_COLLECT_FREE_CON_THRESHOLD)) {

        if (mutex_try_to_acquire(&matched_packets_gc_lock)) {

            time_now_ns = ktime_get_real().tv64;

            hash_for_each_safe(htable, a, tmp, iobj, node) {
                // is the entry old enough to be removed?
                if (time_now_ns - iobj->tstamp_ns >
                    MATCHER_GARBAGE_COLLECT_REQUEST_AGE_THRESHOLD_NS) {

                    hash_del(&iobj->node);

                    reclaim_request_record_container(iobj);

                    gc_count++;
                }
            }

            mutex_unlock(&matched_packets_gc_lock);
        }
        printk(KERN_INFO MATCHER"GC completed, reclaimed [%i] containers\n", gc_count);
    }
}

void send_loadfeedback(struct sk_buff *skb, struct net_device * dev){
    /* send back any available load feedback to all designated hosts*/
    /* Has to be called from the incoming path, see Issue #4*/
    int ret;
    static struct matched_request_responce_feedback mrrf;

    while (!kfifo_is_empty(&fifo_load_feedback_queue)) {
        ret = kfifo_out(&fifo_load_feedback_queue,
                        &mrrf,
                        sizeof(struct matched_request_responce_feedback));

        // printk(KERN_INFO "FIFO dequeuing [%i] bytes [%i][%lld]!\n", ret,
        //     mrrf.dst_ip, mrrf.lfp.total_service_time_ns);

         send_udp_message2same_destination(skb, dev, PORT_LOAD_FEEDBACK,
           (unsigned char *)&mrrf.lfp, sizeof(struct load_feedback_payload));
    }
}

void hash_sample(void){

    printk(KERN_INFO "Has is working...lol\n");
    //Here we define 2^4=16 buckets for the hashes. When the key will be hashed, it will end up in
    // one of these 16 buckets
    DEFINE_HASHTABLE(htable, 4);

    struct request_record_container obj1, obj2, obj3, obj4;

    obj1.request_id=1;
    obj1.tstamp_ns=10;

    obj2.request_id=2;
    obj2.tstamp_ns=20;

    // Note here we have 2 elements with the same ID, which will go into the same bucket
    // Since they are being hashed to the same bucket, they will form a linked list.
    obj4.request_id=2;
    obj4.tstamp_ns=20;

    obj3.request_id=2;
    obj3.tstamp_ns=30;

    // This is how you add each element into the hash table
    hash_add(htable, &obj1.node, obj1.request_id);
    hash_add(htable, &obj2.node, obj2.request_id);
    hash_add(htable, &obj3.node, obj3.request_id);
    hash_add(htable, &obj4.node, obj4.request_id);

    struct request_record_container* iobj;
    int key = 2;
    int a;
    struct hlist_node *tmp;
    hash_for_each_safe(htable, a, tmp, iobj, node) {
        printk(KERN_INFO "HASHX\t bucket=%d => %i, pointer %p\n", a, iobj->tstamp_ns, iobj);
        hash_del(&iobj->node);
    }

    hash_for_each(htable, a, iobj, node) {
        printk(KERN_INFO "HASHY\t bucket=%d => %i, pointer %p\n", a, iobj->tstamp_ns, iobj);
        // hash_del(&iobj->node);
    }


}


