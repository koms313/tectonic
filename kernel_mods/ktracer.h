// 111 -> ms
// 222 -> us
// 333 -> ns
             //     sec|ms|us|ns
             //     000111222333
#define EPOCH_LEN_NS  5000000000

                 //   [sec]111222333
#define TIMER_PRECISION_NS  15000000

/** Tracer communicates via netlink or proc with external world (Path classifier) **/
#define NETLINK_KTRACER_GROUP 32
#define NETLINK_KFILTER_GROUP 31

/** Defines communication method between tct filter+tracer with user space processes**/
/* TODO: currently tracer only works with proc and filter with netlink. Fix tracer to use
netlink */
#undef TRC2USR_COM_VIA_NETLINK
#define TRC2USR_COM_VIA_PROC

#undef FLTR2USR_COM_VIA_NETLINK
#define FLTR2USR_COM_VIA_PROC

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

#define IP_STR_LEN 16
#define MAC_STR_LEN 18

#define REP_LEN 3

#define NET_DEV_NAME "eth0"

// Logging prefixes
#define TRC "[TRC]: "
#define FLTR "[FTR]: "
#define MATCHER "[MCH]: "

#undef INFO

// Unique number (1byte) part of the Tracers TCP sequence number used for identification
#define TCP_SEQ_RQST_MAGIC 77
#define TCP_SEQ_RESP_MAGIC 88
// The first unique number (4byte) in the payload of the tracerouting probe
#define TRC_PROBE_RQST_MAGIC  123321
#define TRC_PROBE_RESP_MAGIC  456654

#define PORT_LOAD_FEEDBACK 17000

#define UNUSED_FIELD 1234

/** This is the datastructure that is returned via netlink to user space listener **/
struct trc_sendr_usr_report {
    uint32_t src_ip;             // Source IP who initiated the trace //TODO: redundant?
    uint32_t dst_ip;             // Destination IP where do we trace to
    uint64_t time_ns;            // Time when probe has been sent out
    uint8_t trace_tcp_seq_id;    // 1-256 trace id
    uint64_t trace_payload_id;   // globally unique trace id //TODO: redundant?
};

/** This is the datastructure that is returned via netlink to user space listener
when kfilter receives back itrs trace probe from the remote kfilter.
Message will be autoalligned, no need to use fields smaller than 32 **/
#define TRC_BOUNCED_PROBE 10
struct trc_bounce_usr_report {
    uint32_t type;              // Either ICMP TE or Bounced probe
    uint32_t tcp_seq;         // TCP Sequence that was set to the probe
    uint32_t src_ip;           // Source IP who initiated the trace (should be "us") //TODO: redundant?
    uint32_t dst_ip;           // Who returned the probe.
    uint64_t bounce_time_ns;   // Remote timestamp when the message was bounced back.
    uint64_t rcv_time_ns;      // When we received the message back.
    uint64_t rtt_ns;           // RTT based on the timestamp in the probe's payload
};

#define TRC_ICMPTE_PROBE 20
struct trc_icmpte_usr_report {
    uint32_t type;             // Either ICMP TE or Bounced probe
    uint32_t tcp_seq;         // TCP Sequence that was set to the probe
    uint32_t src_ip;          // TODO: redundant?
    uint32_t dst_ip;          // TODO: redundant?
    uint32_t hop_ip;          // IP of the network device that replied to us.
    uint64_t rcv_time_ns;     // When we received the message back.
};

struct trc_probe_payload {
    uint32_t probe_magic;
    uint32_t type;
    uint64_t send_time_ns;
    uint64_t bounce_time_ns;
    uint64_t trace_payload_id;
};

//////////////////////////////////////////////////////////////////////////////////////////
/// LOAD PROBES //////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

struct load_feedback_payload {
    uint64_t total_service_time_ns; // Time a request spent on the host from the time it
                                    // arrived, until the moment response was emitted back
};

/* When a request is being positively matched with the response it is stored in a fifo
queue before being sent out. The entry contains the entire information needed + dst_ip to
where send the data */
struct matched_request_responce_feedback {
    struct load_feedback_payload lfp;
    uint32_t dst_ip;
};

//////////////////////////////////////////////////////////////////////////////////////////
/// YCSB /////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
// See the spec https://github.com/apache/cassandra/blob/trunk/doc/native_protocol_v4.spec
struct ycsb_msg_payload {
    uint8_t version;
    uint8_t flags;
    /* There are no absolutely unique request ids, there is only a stream ID that is
    unique for a given flow (connection) for the duration of the response to get back.
    In other words, when a response is received by the driver, it can send a new request
    with the same stream id, thus it is not an absolute identifier and timestamp needs
    to be used when matching requests and responses. Alternatively it is possible to
    configure YCSB (or java driver) to have 1 thread per connection, which allows for
    unique identification when combined with src port (i.e., no possible for missed
    packets due to TCP segment coalescence) */
    uint16_t stream_id;
    uint8_t opcode;
};

/* Note Hex 84 is 132 in decimal */
#define YCSB_V4_REQUEST 4
#define YCSB_V4_RESPONSE 132

#define YCSB_OPCODE_QUERY 7
#define YCSB_OPCODE_RESULT 8

