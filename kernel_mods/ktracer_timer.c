#include <linux/etherdevice.h>
#include <linux/fs.h>
#include <linux/icmp.h>
#include <linux/if_arp.h>
#include <linux/if_ether.h>
#include <linux/inet.h>
#include <linux/io.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/netdevice.h>
#include <linux/netdevice.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/slab.h>
#include <linux/timer.h>
#include <linux/tcp.h>
#include <linux/inetdevice.h>



#include <net/checksum.h>

// Netlink
#include <net/netlink.h>
#include <net/net_namespace.h>
#include <linux/netlink.h>

#include "ktracer.h"

// #define INFO

MODULE_LICENSE("GPL");

/* ktracer_ip.c  ************************************************************************/
extern const char dst_ips_str[REP_LEN][IP_STR_LEN];
extern const char local_ip_pub_str[IP_STR_LEN];
extern const char local_ip_priv_str[IP_STR_LEN];
extern const char next_hop_mac_str[REP_LEN][MAC_STR_LEN];
extern unsigned char next_hop_mac_num[REP_LEN][6];
extern uint32_t dst_ips_num[REP_LEN];
extern uint32_t local_ip_pub_num;
extern uint32_t local_ip_priv_num;
extern uint32_t local_ip_id;
void prepare_ip_lists(void);


/* ktracer_nlink.c **********************************************************************/
struct sock * init_netlink_socket(void);
int send2usr_via_netlink(
    struct sock * s, uint32_t net_link_group, unsigned char * data, int data_len);
// int send2usr_trc_snd_report(
//     struct sock * s, uint32_t net_link_group, struct trc_sendr_usr_report * user_msg);

/* ktracer_probe.c **********************************************************************/
uint32_t pack_tcp_seq(uint8_t ttl, uint8_t trace_tcp_seq_id);
void assemble_trc2usr_snd_report(struct trc_sendr_usr_report * uspace_msg,
    uint32_t src_ip, uint32_t dst_ip, uint64_t time_ns,
    uint8_t trace_tcp_seq_id, uint64_t trace_payload_id);


/* ktracer_netasm.c *********************************************************************/
struct net_device * get_net_device(const char * dev_name);
struct sk_buff* construct_tcp_skb(struct net_device *dev,
    unsigned char * src_mac, unsigned char * dst_mac,
    uint32_t src_ip, uint32_t dst_ip,
    uint32_t src_port, uint32_t dst_port,
    uint32_t ttl, uint32_t tcp_seq,
    char * usr_data, uint16_t usr_data_len);

/* ktracer_proc.c ***********************************************************************/
int setup_proc_sending_traces(void);
void remove_proc_sending_traces(void);
int log2proc_ktracer_sender_msg(struct trc_sendr_usr_report * user_msg);



// This is the offset interval between all rounds.
uint64_t round_len_ns;

// This is the time when the first epoch started.
uint64_t epoch_zero_time;

// Epoch ID and round ID in the current epoch
uint64_t eid = 0;
uint64_t rid = 0;


/* Loosy identifier of the trace. Each trace sent from this node to other nodes, has a
an id in range 0->256. Looping around. ICMP probes can take time to come back. In
combination with sent time, this should be enough to match all returned probes */
uint8_t trace_tcp_seq_id = 0;

/* This is truly unique identifier matched between sender and receiver. ICMP probes
do not contain this data */
uint64_t trace_payload_id = 0;



struct net_device *dev;

struct timer_list ktimer;

struct sock * nlink_sender_sock = NULL;




/** From the time now, returns the amount of time until the next new epoch **/
inline uint64_t get_nsec2next_epoch(uint64_t epoch_len_ns) {
    return epoch_len_ns - (ktime_get_real().tv64 % epoch_len_ns);
}
inline uint64_t get_usec2next_epoch(uint64_t epoch_len_ns) {
    return (epoch_len_ns - (ktime_get_real().tv64 % epoch_len_ns)) / 1000 ;
}
inline uint64_t get_msec2next_epoch(uint64_t epoch_len_ns) {
    return (epoch_len_ns - (ktime_get_real().tv64 % epoch_len_ns)) / 1000000 ;
}

/** This is the exact (expected) time when the epoch zero should begin
While we start with the real time now, we remove the remainder and compute nearest
future time that is multiple of the epoch length
**/
inline uint64_t compute_epoch_zero_time(uint64_t epoch_len_ns) {
    uint64_t time_now = ktime_get_real().tv64;
    return (time_now - (time_now % epoch_len_ns)) + epoch_len_ns;
}

inline uint64_t get_expected_time(void) {
    return epoch_zero_time + EPOCH_LEN_NS*eid + round_len_ns*rid;
}






void get_local_ipv4(struct net_device *dev){

    struct in_device* in_dev;
    struct in_ifaddr* if_info;

    in_dev = (struct in_device *)dev->ip_ptr;
    if_info = in_dev->ifa_list;

        for (;if_info;if_info=if_info->ifa_next)
            {

            printk(KERN_INFO "next\n");

            // snprintf(source, 16, "%pI4", &if_info->ifa_address);


            printk(KERN_INFO "if_info->ifa_address=%pI4\n", &if_info->ifa_address);

                // break;

            }
}








void launch_trace_probes_to(uint64_t dst_id){

    static struct trc_sendr_usr_report uspace_msg;
    struct sk_buff *skb;
    int ttl_iter = 0;
    static struct trc_probe_payload pp;

    trace_tcp_seq_id += 1;
    trace_payload_id += 1;


    pp.probe_magic = TRC_PROBE_RQST_MAGIC;
    pp.type = 1;
    pp.send_time_ns = ktime_get_real().tv64;
    pp.bounce_time_ns = 0;
    pp.trace_payload_id = trace_payload_id;


    #ifdef TEC_OPERATION_PING
    // We send a single probe, but it has to reach the destination
    ttl_iter = 128;
    {
    #endif
    #ifdef TEC_OPERATION_TRACEROUTE

    for (ttl_iter=1; ttl_iter<30; ttl_iter++) {

    #endif

        // log2proc_ktracer_sender_msg(tcp_seq, ktime_get_real().tv64);

        skb = construct_tcp_skb(dev,
           dev->dev_addr,  &next_hop_mac_num[local_ip_id][0],
           local_ip_priv_num, dst_ips_num[dst_id],
           53322, 7000,
           ttl_iter, pack_tcp_seq(ttl_iter, trace_tcp_seq_id),
           (unsigned char *)&pp, sizeof(struct trc_probe_payload)
           );

        if (dev_queue_xmit(skb) != NET_XMIT_SUCCESS) {
            printk(KERN_INFO TRC"[XMIT] Sending failed\n");
        }
    }

    assemble_trc2usr_snd_report(&uspace_msg,
        local_ip_pub_num, dst_ips_num[dst_id], ktime_get_real().tv64,
        trace_tcp_seq_id, trace_payload_id);

    #ifdef TRC2USR_COM_VIA_NETLINK
        send2usr_trc_snd_report();
        send2usr_via_netlink(nlink_sender_sock, NETLINK_KTRACER_GROUP,
            &uspace_msg, sizeof(struct trc_sendr_usr_report));
    #endif

    #ifdef TRC2USR_COM_VIA_PROC
        log2proc_ktracer_sender_msg(&uspace_msg);
    #endif


}



void launch_trace_probes(void) {
    /** Based on the rid we either send probes to a single destination or to all nodes
    at the same time **/
    int i;

    if (local_ip_pub_num != dst_ips_num[rid]) {
        //printk(KERN_INFO TRC"Sending Trace Single [e/r] [%lld/%lld] [ids: %i -> %lld]\n",
        //    eid, rid, local_ip_id, rid);
        launch_trace_probes_to(rid);
    } else {
        // We need to send synchronous probes to all other nodes
        for (i = 0; i < REP_LEN; i++) {
            if (local_ip_pub_num != dst_ips_num[i]) {
                //printk(KERN_INFO TRC"Sending Trace Multiple [e/r] [%lld/%lld] [ids: %i -> %i]\n",
                //eid, rid, local_ip_id, i);
                launch_trace_probes_to(i);
            }
        }
    }
}




void ktimer_routine(unsigned long data1) {

    static uint64_t sleep_time_us;
    static uint64_t real_time_now;

    // if (eid == 0) { // first epoch
        // epoch_zero_time = ktime_get_real().tv64;
        // last_time = ktime_get_real().tv64;
    // }


    // static uint64_t expected_time_now;

    #ifdef INFO
        printk(KERN_INFO "Iteration [e/r: %lld/%lld] \n", eid, rid);
    #endif

    real_time_now = ktime_get_real().tv64;

    // expected_time_now = get_expected_time();



    while (get_expected_time() + TIMER_PRECISION_NS < real_time_now) {
        // we are lagging behind, the round/epoch id point to the time in the past
        // fast forward to the next iteration

        printk(KERN_INFO "Skipping [e/r] [%lld/%lld] [lag: %lld ms][epoch_len: %ld ms] %i\n",
            eid, rid, (real_time_now - get_expected_time()) / 1000000, EPOCH_LEN_NS / 1000000, HZ ) ;
        rid++;

        if (rid >= REP_LEN) {
            rid = 0;
            eid++;
        }
    }


    /* Main logic here */
    launch_trace_probes();



    rid++;

    if (rid >= REP_LEN) {
        /** There are no more rounds for this epoch, move to the next epoch **/
        rid = 0;

        eid++;

        /* restarting timer */
        sleep_time_us = get_usec2next_epoch(EPOCH_LEN_NS);
        #ifdef INFO
            printk(KERN_INFO "Rescheduling for next epoch [%lldms]\n", sleep_time_us / 1000);
        #endif
        mod_timer(&ktimer, jiffies + usecs_to_jiffies(sleep_time_us));
    } else {
        /** Skip until the next round **/

        sleep_time_us = (get_expected_time() - ktime_get_real().tv64) / 1000;
        #ifdef INFO
            printk(KERN_INFO "Rescheduling for next round [%lldms]\n", sleep_time_us / 1000);
        #endif
        mod_timer(&ktimer, jiffies + usecs_to_jiffies(sleep_time_us));
    }


}


static int timer_module_init(void){

    #ifdef TRC2USR_COM_VIA_PROC
        setup_proc_sending_traces();
    #endif

    #ifdef TRC2USR_COM_VIA_NETLINK
        nlink_sender_sock =  init_netlink_socket();
        if (!nlink_sender_sock) {
            return -1;
        }
    #endif

    prepare_ip_lists();

    dev = get_net_device(NET_DEV_NAME);
    if (!dev) {
        printk(KERN_INFO "KTimer Cannot find net device\n");
    }

    round_len_ns = EPOCH_LEN_NS / REP_LEN;
    epoch_zero_time = compute_epoch_zero_time(EPOCH_LEN_NS);



    init_timer(&ktimer);
    ktimer.function = ktimer_routine;
    ktimer.data = 1;
    ktimer.expires = jiffies + usecs_to_jiffies(get_usec2next_epoch(EPOCH_LEN_NS));
    add_timer(&ktimer); /* Starting the timer */

    printk(KERN_INFO TRC"KTracer Module loaded\n");
    return 0;
}

static void timer_module_exit(void) {
    del_timer_sync(&ktimer); /* Deleting the timer */

    #ifdef TRC2USR_COM_VIA_PROC
        remove_proc_sending_traces();
    #endif

    #ifdef TRC2USR_COM_VIA_NETLINK
        netlink_kernel_release(nlink_sender_sock);
    #endif

    printk(KERN_INFO TRC"KRacer module unloaded \n");
}

module_init(timer_module_init);
module_exit(timer_module_exit);