#include <linux/sort.h>
#include <linux/inet.h>

#include "ktracer.h"


unsigned char next_hop_mac_num[REP_LEN][6];

extern const char dst_ips_str[REP_LEN][IP_STR_LEN];
extern const char local_ip_pub_str[IP_STR_LEN];
extern const char local_ip_priv_str[IP_STR_LEN];
extern const char next_hop_mac_str[REP_LEN][MAC_STR_LEN];


uint32_t dst_ips_num[REP_LEN];
uint32_t local_ip_pub_num;
uint32_t local_ip_id; /* our ip index position in the global list of ips */
uint32_t local_ip_priv_num;

extern void mac_string_to_bites(unsigned char * mac, const unsigned char * mac_str);
extern void printk_mac(unsigned char * mac[6]);
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

static int compare(const void *lhs, const void *rhs) {
    int lhs_integer = *(const uint32_t *)(lhs);
    int rhs_integer = *(const uint32_t *)(rhs);

    if (lhs_integer < rhs_integer) return -1;
    if (lhs_integer > rhs_integer) return 1;
    return 0;
}

void convert_ips_str2num(void) {
    int i;

    local_ip_pub_num = in_aton(local_ip_pub_str);
    local_ip_priv_num = in_aton(local_ip_priv_str);

    for (i=0; i < REP_LEN; i++) {
        dst_ips_num[i] = in_aton(dst_ips_str[i]);

        // find ourselves in the global list of ips
        if (local_ip_pub_num == dst_ips_num[i]) {
            local_ip_id = i;
        }
    }




}

void convert_macs_str2num(void) {

    int i;

    for (i=0; i < REP_LEN; i++) {
        mac_string_to_bites(&next_hop_mac_num[i][0], &next_hop_mac_str[i][0]);
        // printk_mac(next_hop_mac_num[i][0]);

        // printk(KERN_INFO "[%p][%02x]\n", &next_hop_mac_num[i][0], next_hop_mac_num[i][0]);
        // for (j=0; j < 6; j++) {
        //     printk(KERN_INFO "\t[%p][%u]\n", &next_hop_mac_num[i][j], next_hop_mac_num[i][j]);
        // }

    }

}


void prepare_ip_lists(void) {
    convert_ips_str2num();

    convert_macs_str2num();


    sort(dst_ips_num, REP_LEN, sizeof(uint32_t), &compare, NULL);

    // for (i=0; i < REP_LEN; i++) {
    //     printk(KERN_ALERT"[%s][%u]\n", dst_ips_str[i], dst_ips_num[i]);
    // }
}

