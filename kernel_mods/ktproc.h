// Each buffer should be of a page size.
#define PROC_BUFF_SIZE 4095
#define MAX_BUFFS 20000

#define PROC_IN_LOG_FNAME "ktin.log"
#define PROC_OUT_LOG_FNAME "ktout.log"

#define PROC_KTRACER_SENDER_FNAME "ktracer.sender.log"
#define PROC_KTRACER_BOUNCER_FNAME "ktracer.bounce.log"
#define PROC_KTRACER_RECEIVER_FNAME "ktracer.receiver.log"
#define PROC_KTRACER_LOADFEEDBACK_FNAME "ktracer.loadfeedback.log"


#define KTEC "[TEC]: "
// #define LOG_IN
// #define LOG_OUT

struct text_container {
    char * data;
    int max_size;
    int text_size;

    // Each container has a pointer to a global index, one per sequence file (i.e., in/out)
    // This is done so that we can reset it from the sequence_stop function (generically)
    // We can distinguish between in/out directions by blindly resetting the pointer
    int * global_con_index;

    int id;
};



