// #include <linux/module.h>
// #include <linux/moduleparam.h>
// #include <linux/init.h>
// #include <linux/kernel.h>
// #include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
// #include <linux/slab.h>

#include "ktproc.h"
#include "ktracer.h"

int init_con_list(struct text_container *** container_list, int * container_index);
void free_con_list(struct text_container *** container_list);
int setup_proc_file(struct proc_dir_entry * entry, const char * proc_file_name, struct file_operations * file_ops);

static struct  text_container ** incoming_con_list;
static struct  text_container ** outgoing_con_list;
/* This file contains messages generated when our trace probe bounce back from remote
(sent back) to the original sender */
static struct  text_container ** ktracer_bounce_con_list;
/* -//- messages when ICMP Time Exceeded packets from remote are returned to "us" */
static struct  text_container ** ktracer_recv_con_list;

static struct  text_container ** ktracer_loadfeedback_con_list;

static int in_con_index;
static int out_con_index;
static int ktracer_bounce_con_index;
static int ktracer_recv_con_index;
static int ktracer_loadfeedback_con_index;

extern struct seq_operations ct_seq_ops;

struct proc_dir_entry * proc_entry_in;
struct proc_dir_entry * proc_entry_out;

struct proc_dir_entry * proc_entry_ktracer_bounce;
struct proc_dir_entry * proc_entry_ktracer_recv;
struct proc_dir_entry * proc_entry_ktracer_loadfeedback;


int log2proc_timestamp_msg(
        struct  text_container ** ppcon_list, int * con_index, uint32_t tstmp_order,
        uint32_t msg_id, uint32_t verb_opcode, ktime_t tstamp, uint32_t reference_ip,
        uint16_t reference_port) {

    static int append_len;
    #ifdef DEBUG_PROC
        static char msg[100];
    #endif

    struct  text_container * con = ppcon_list[*con_index];


    // If we have enough of space for a message
    if (con->max_size - con->text_size < 50) {

        if (*con_index + 1 < MAX_BUFFS) {
            *con_index += 1;
            con = ppcon_list[*con_index];
            // printk(KERN_INFO "Moving to the next container index[%i]\n", *con_index);

            if (con->text_size > 0) {
                printk(KERN_ERR "ERROR new container is not empty !\n");
            }
        } else {
            // We used up all our preallocated buffers
            // printk(KERN_INFO "No more containers to dump date\n");
            return -1;
        }
    }


    append_len = sprintf(con->data + con->text_size,
        "%u %u 0 %lld %u %u\n",
        tstmp_order, msg_id, tstamp.tv64, reference_ip, reference_port);

    #ifdef DEBUG_PROC
        sprintf(msg,
            "%u %u %u %lld %pI4 %u\n",
            tstmp_order, msg_id, verb_opcode, tstamp.tv64, &reference_ip, &reference_port);
        printk(KERN_INFO "proc: %s\n", msg);
    #endif

    if (append_len < 0) {
        printk(KERN_INFO "Error appending text to the buffer\n");
        return -1;
    } else {
        con->text_size += append_len;
    }

    return 0;

}

inline int log2proc_incoming_packet(
        uint32_t tstmp_order, uint32_t msg_id, uint32_t verb_opcode, ktime_t tstamp,
        uint32_t reference_ip, uint16_t src_port) {

    return log2proc_timestamp_msg(incoming_con_list, &in_con_index,
        tstmp_order, msg_id, verb_opcode, tstamp, reference_ip, src_port);

}

inline int log2proc_outgoing_packet(
        uint32_t tstmp_order, uint32_t msg_id, uint32_t verb_opcode, ktime_t tstamp,
        uint32_t reference_ip, uint16_t dst_port) {

    return log2proc_timestamp_msg(outgoing_con_list, &out_con_index,
        tstmp_order, msg_id, verb_opcode, tstamp, reference_ip, dst_port);

}



inline int log2proc_returning_icmp_ttl_exceeded(
    struct trc_icmpte_usr_report * uspace_msg) {

    static int append_len;
    #ifdef DEBUG_PROC
        static char msg[100];
    #endif

    struct  text_container * con = ktracer_recv_con_list[ktracer_recv_con_index];

    // If we have enough of space for a message
    if (con->max_size - con->text_size < 100) {


        if (ktracer_recv_con_index + 1 < MAX_BUFFS) {
            ktracer_recv_con_index += 1;
            con = ktracer_recv_con_list[ktracer_recv_con_index];
            // printk(KERN_INFO "Moving to the next container index[%i]\n", *con_index);

            if (con->text_size > 0) {
                printk(KERN_ERR "ERROR new container is not empty !\n");
            }


        } else {
            // We used up all our preallocated buffers
            // printk(KERN_INFO "No more containers to dump date\n");
            return -1;
        }
    }

    append_len = sprintf(con->data + con->text_size,
        "%u %u %u %u %u %lld\n",
        uspace_msg->type,
        uspace_msg->tcp_seq,
        uspace_msg->src_ip,
        uspace_msg->dst_ip,
        uspace_msg->hop_ip,
        uspace_msg->rcv_time_ns);

    #ifdef DEBUG_PROC
        sprintf(msg, "%u %u %u %u %u %lld\n",
        uspace_msg->type,
        uspace_msg->tcp_seq,
        uspace_msg->src_ip,
        uspace_msg->dst_ip,
        uspace_msg->hop_ip,
        uspace_msg->rcv_time_ns);
    #endif


    if (append_len < 0) {
        printk(KERN_INFO "Error appending text to the buffer\n");
        return -1;
    } else {
        con->text_size += append_len;
    }

    return 0;
}


inline int log2proc_bounced_probe_exceeded(
    struct trc_bounce_usr_report * uspace_msg) {

    static int append_len;
    #ifdef DEBUG_PROC
        static char msg[100];
    #endif

    struct  text_container * con = ktracer_bounce_con_list[ktracer_bounce_con_index];

    // If we have enough of space for a message
    if (con->max_size - con->text_size < 100) {


        if (ktracer_bounce_con_index + 1 < MAX_BUFFS) {
            ktracer_bounce_con_index += 1;
            con = ktracer_bounce_con_list[ktracer_bounce_con_index];
            // printk(KERN_INFO "Moving to the next container index[%i]\n", *con_index);

            if (con->text_size > 0) {
                printk(KERN_ERR "ERROR new container is not empty !\n");
            }


        } else {
            // We used up all our preallocated buffers
            // printk(KERN_INFO "No more containers to dump date\n");
            return -1;
        }
    }

    append_len = sprintf(con->data + con->text_size,
        "%u %u %u %u %lld %lld %lld\n",
        uspace_msg->type,
        uspace_msg->tcp_seq,
        uspace_msg->src_ip,
        uspace_msg->dst_ip,
        uspace_msg->bounce_time_ns,
        uspace_msg->rcv_time_ns,
        uspace_msg->rtt_ns);

    #ifdef DEBUG_PROC
        sprintf(msg, "%u %u %u %u %lld %lld %lld\n",
        uspace_msg->type,
        uspace_msg->tcp_seq,
        uspace_msg->src_ip,
        uspace_msg->dst_ip,
        uspace_msg->bounce_time_ns,
        uspace_msg->rcv_time_ns,
        uspace_msg->rtt_ns);
    #endif


    if (append_len < 0) {
        printk(KERN_INFO "Error appending text to the buffer\n");
        return -1;
    } else {
        con->text_size += append_len;
    }

    return 0;
}

void log2proc_load_feedback(struct load_feedback_payload * lfb, uint32_t src_ip){

    static int append_len;
    #ifdef DEBUG_PROC
        static char msg[100];
    #endif

    struct  text_container * con = ktracer_loadfeedback_con_list[ktracer_loadfeedback_con_index];

    // If we have enough of space for a message
    if (con->max_size - con->text_size < 100) {


        if (ktracer_loadfeedback_con_index + 1 < MAX_BUFFS) {
            ktracer_loadfeedback_con_index += 1;
            con = ktracer_loadfeedback_con_list[ktracer_loadfeedback_con_index];
            // printk(KERN_INFO "Moving to the next container index[%i]\n", *con_index);

            if (con->text_size > 0) {
                printk(KERN_ERR "ERROR new container is not empty !\n");
            }
        } else {
            // We used up all our preallocated buffers
            // printk(KERN_INFO "No more containers to dump date\n");
            return -1;
        }
    }

    append_len = sprintf(con->data + con->text_size,
        "%u %u\n",
        src_ip,
        lfb->total_service_time_ns);

    #ifdef DEBUG_PROC
        sprintf(msg, "%u %lld\n",
        src_ip,
        lfb->total_service_time_ns);
    #endif


    if (append_len < 0) {
        printk(KERN_INFO "Error appending text to the buffer\n");
        return -1;
    } else {
        con->text_size += append_len;
    }

    return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////

static int open_proc_in(struct inode *inode, struct file *file) {

    struct seq_file * sf;
    int res =  seq_open(file, &ct_seq_ops);
    if (res < 0) return res;

    sf = (struct seq_file*)file->private_data;
    sf->private = incoming_con_list;

    return res;
};

static int open_proc_out(struct inode *inode, struct file *file) {

    struct seq_file * sf;
    int res =  seq_open(file, &ct_seq_ops);
    if (res < 0) return res;

    sf = (struct seq_file*)file->private_data;
    sf->private = outgoing_con_list;

    return res;
};

static int open_proc_ktracer_bounce(struct inode *inode, struct file *file) {

    struct seq_file * sf;
    int res =  seq_open(file, &ct_seq_ops);
    if (res < 0) return res;

    sf = (struct seq_file*)file->private_data;
    sf->private = ktracer_bounce_con_list;

    return res;
};

static int open_proc_ktracer_recv(struct inode *inode, struct file *file) {

    struct seq_file * sf;
    int res =  seq_open(file, &ct_seq_ops);
    if (res < 0) return res;

    sf = (struct seq_file*)file->private_data;
    sf->private = ktracer_recv_con_list;

    return res;
};


static int open_proc_ktracer_loadfeedback(struct inode *inode, struct file *file) {

    struct seq_file * sf;
    int res =  seq_open(file, &ct_seq_ops);
    if (res < 0) return res;

    sf = (struct seq_file*)file->private_data;
    sf->private = ktracer_loadfeedback_con_list;

    return res;
};

/* These structures are only differ by the .open action as they need to pass
   individual pointers to their containers */
static struct file_operations file_inlog_ops = {
    .owner   = THIS_MODULE,
    .open    = open_proc_in,
    .read    = seq_read,
    .llseek  = seq_lseek,
    .release = seq_release
};

static struct file_operations file_outlog_ops = {
    .owner   = THIS_MODULE,
    .open    = open_proc_out,
    .read    = seq_read,
    .llseek  = seq_lseek,
    .release = seq_release
};

static struct file_operations file_ktracer_bounce_ops = {
    .owner   = THIS_MODULE,
    .open    = open_proc_ktracer_bounce,
    .read    = seq_read,
    .llseek  = seq_lseek,
    .release = seq_release
};

static struct file_operations file_ktracer_recv_ops = {
    .owner   = THIS_MODULE,
    .open    = open_proc_ktracer_recv,
    .read    = seq_read,
    .llseek  = seq_lseek,
    .release = seq_release
};

static struct file_operations file_ktracer_loadfeedback_ops = {
    .owner   = THIS_MODULE,
    .open    = open_proc_ktracer_loadfeedback,
    .read    = seq_read,
    .llseek  = seq_lseek,
    .release = seq_release
};

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////




int setup_proc_incoming_log(void) {
    int ret;

    ret = init_con_list(&incoming_con_list, &in_con_index);
    if (ret < 0) return ret;

    ret = setup_proc_file(proc_entry_in, PROC_IN_LOG_FNAME, &file_inlog_ops);

    printk(KERN_INFO "Initializing /proc entry for [%s]\n", PROC_IN_LOG_FNAME);
    return ret;
}


int setup_proc_outgoing_log(void) {
    int ret;

    ret = init_con_list(&outgoing_con_list, &out_con_index);
    if (ret < 0) return ret;

    ret = setup_proc_file(proc_entry_out, PROC_OUT_LOG_FNAME, &file_outlog_ops);

    printk(KERN_INFO "Initializing /proc entry for [%s]\n", PROC_OUT_LOG_FNAME);
    return ret;
}


int setup_proc_ktracer_bounce_log(void) {
    int ret;

    ret = init_con_list(&ktracer_bounce_con_list, &ktracer_bounce_con_index);
    if (ret < 0) return ret;

    ret = setup_proc_file(proc_entry_ktracer_bounce, PROC_KTRACER_BOUNCER_FNAME, &file_ktracer_bounce_ops);

    printk(KERN_INFO "Initializing /proc entry for [%s]\n", PROC_KTRACER_BOUNCER_FNAME);
    return ret;
}


int setup_proc_ktracer_recv(void) {
    int ret;

    ret = init_con_list(&ktracer_recv_con_list, &ktracer_recv_con_index);
    if (ret < 0) return ret;

    ret = setup_proc_file(proc_entry_ktracer_recv, PROC_KTRACER_RECEIVER_FNAME, &file_ktracer_recv_ops);

    printk(KERN_INFO "Initializing /proc entry for [%s]\n", PROC_KTRACER_RECEIVER_FNAME);
    return ret;
}


int setup_proc_ktracer_loadfeedback(void) {
    int ret;

    ret = init_con_list(&ktracer_loadfeedback_con_list, &ktracer_loadfeedback_con_index);
    if (ret < 0) return ret;

    ret = setup_proc_file(proc_entry_ktracer_loadfeedback, PROC_KTRACER_LOADFEEDBACK_FNAME, &file_ktracer_loadfeedback_ops);

    printk(KERN_INFO "Initializing /proc entry for [%s]\n", PROC_KTRACER_LOADFEEDBACK_FNAME);
    return ret;
}



void remove_proc_incoming_log(void) {
    printk(KERN_INFO "Removing /proc entry for [%s]\n", PROC_IN_LOG_FNAME);

    remove_proc_entry(PROC_IN_LOG_FNAME, NULL);

    free_con_list(&incoming_con_list);
}

void remove_proc_outgoing_log(void) {
    printk(KERN_INFO "Removing /proc entry for [%s]\n", PROC_OUT_LOG_FNAME);

    remove_proc_entry(PROC_OUT_LOG_FNAME, NULL);

    free_con_list(&outgoing_con_list);
}

void remove_proc_ktracer_bounce(void) {
    printk(KERN_INFO "Removing /proc entry for [%s]\n", PROC_KTRACER_BOUNCER_FNAME);

    remove_proc_entry(PROC_KTRACER_BOUNCER_FNAME, NULL);

    free_con_list(&ktracer_bounce_con_list);
}

void remove_proc_ktracer_recv(void) {
    printk(KERN_INFO "Removing /proc entry for [%s]\n", PROC_KTRACER_RECEIVER_FNAME);

    remove_proc_entry(PROC_KTRACER_RECEIVER_FNAME, NULL);

    free_con_list(&ktracer_recv_con_list);
}

void remove_proc_ktracer_loadfeedback(void) {
    printk(KERN_INFO "Removing /proc entry for [%s]\n", PROC_KTRACER_LOADFEEDBACK_FNAME);

    remove_proc_entry(PROC_KTRACER_LOADFEEDBACK_FNAME, NULL);

    free_con_list(&ktracer_loadfeedback_con_list);
}