#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/netfilter.h>
#include <linux/netdevice.h>
#include <linux/netfilter_ipv4.h>
#include <linux/if_ether.h>

static struct nf_hook_ops nfin;         //struct holding set of hook function options

static struct nf_hook_ops nfout;         //struct holding set of hook function options



static unsigned int hook_func_in(unsigned int hooknum,
            struct sk_buff *skb,
                            const struct net_device *in,
                            const struct net_device *out,
                            int (*okfn)(struct sk_buff *))
{
    struct ethhdr *eth;
    struct iphdr *ip_header;
    // /* check *in is the correct device */
    // // if (in is not the correct device)
    //       return NF_ACCEPT;



    if (in->ifindex == 3) {

        eth = (struct ethhdr*)skb_mac_header(skb);
        ip_header = (struct iphdr *)skb_network_header(skb);


        printk("[IN ][ifindex:%i] src mac %pM, dst mac %pM, len: %i  [%ld]\n", in->ifindex, eth->h_source, eth->h_dest, skb->len, skb->tstamp.tv64);
    }



    // printk("KBG message: [%lu] \n", skb->tstamp.tv64);
    // printk("src IP addr:=%d.%d.%d.%d:%d\n", NIPQUAD_FMT(ip_header->saddr));
    return NF_ACCEPT;
}



static unsigned int hook_func_out(unsigned int hooknum,
            struct sk_buff *skb,
                            const struct net_device *in,
                            const struct net_device *out,
                            int (*okfn)(struct sk_buff *))
{
    struct ethhdr *eth;
    struct iphdr *ip_header;
    // /* check *in is the correct device */
    // // if (in is not the correct device)
    //       return NF_ACCEPT;



    if (out->ifindex == 3) {

        eth = (struct ethhdr*)skb_mac_header(skb);
        ip_header = (struct iphdr *)skb_network_header(skb);


        printk("[OUT][ifindex:%i] src mac %pM, dst mac %pM, len: %i  [%ld]\n", out->ifindex, eth->h_source, eth->h_dest, skb->len, skb->tstamp.tv64);
    }


    // printk("KBG message: [%lu] \n", skb->tstamp.tv64);
    // printk("src IP addr:=%d.%d.%d.%d:%d\n", NIPQUAD_FMT(ip_header->saddr));
    return NF_ACCEPT;
}


static int __init init_main(void)
{
    nfin.hook     = hook_func_in;
    // nfin.hooknum  = NF_INET_LOCAL_OUT;
    nfin.hooknum  = NF_INET_PRE_ROUTING;
    nfin.pf       = PF_INET;
    nfin.priority = NF_IP_PRI_FIRST;
    nf_register_hook(&nfin);


    nfout.hook     = hook_func_out;
    // nfin.hooknum  = NF_INET_LOCAL_OUT;
    nfout.hooknum  = NF_INET_POST_ROUTING;
    nfout.pf       = PF_INET;
    nfout.priority = NF_IP_PRI_FIRST;
    nf_register_hook(&nfout);

    return 0;
}


static void __exit cleanup_main(void)
{
    nf_unregister_hook(&nfin);
}
module_init(init_main);
module_exit(cleanup_main);