#include <linux/kernel.h>
// #include <linux/tcp.h>

#include "ktracer.h"

// extern struct tcphdr;

inline uint32_t pack_tcp_seq(uint8_t ttl, uint8_t trace_tcp_seq_id) {
    static uint32_t seq_number;
    static uint8_t * p;

    seq_number = 0;
    p = (uint8_t *)&seq_number;


    *p = (uint8_t)TCP_SEQ_RQST_MAGIC;
    *(p+1) = ttl;
    *(p+2) = trace_tcp_seq_id;
    *(p+3) = 123;

    return seq_number;
}

inline uint32_t unpack_tcp_seq_magic(uint32_t seq_number) {
    /** use ntohl if reading from network **/

	static void * p_seq_number;
    p_seq_number = (void*)&seq_number;
    return (uint32_t)(*(uint8_t*)p_seq_number);
}


inline int is_incoming_trc_rqst(uint32_t tcph_seq, uint32_t * tcp_payload) {
    /* Checks if packet is an incoming tracer's probe
       1. Check tcph sequence number to have a magic number
       2. Check first 4bytes in the payload to be probe's magic number */
    static struct trc_probe_payload * probe_payload;
    probe_payload = (struct trc_probe_payload *)tcp_payload;

    if ( (unpack_tcp_seq_magic(ntohl(tcph_seq)) == TCP_SEQ_RQST_MAGIC) &&
         (probe_payload->probe_magic == TRC_PROBE_RQST_MAGIC) ) {
        return 1;
    }

    return 0;
}

inline int is_incoming_trc_resp(uint32_t tcph_seq, uint32_t * tcp_payload) {
    static struct trc_probe_payload * probe_payload;
    probe_payload = (struct trc_probe_payload *)tcp_payload;

    if ( (unpack_tcp_seq_magic(ntohl(tcph_seq)) == TCP_SEQ_RESP_MAGIC) &&
         ( probe_payload->probe_magic == TRC_PROBE_RESP_MAGIC) ) {
        return 1;
    }

    return 0;
}


/** This function construct a structure that will be sent to user space via netlink
This structure contains the information about the trace probe that just had been sent out **/
void assemble_trc2usr_snd_report(struct trc_sendr_usr_report * uspace_msg,
    uint32_t src_ip, uint32_t dst_ip, uint64_t time_ns,
    uint8_t trace_tcp_seq_id, uint64_t trace_payload_id) {

    uspace_msg->src_ip = src_ip;
    uspace_msg->dst_ip = dst_ip;
    uspace_msg->time_ns = time_ns;
    uspace_msg->trace_tcp_seq_id = trace_tcp_seq_id;
    uspace_msg->trace_payload_id = trace_payload_id;
}