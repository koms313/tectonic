#include <linux/inet.h> // in_aton
#include <linux/inetdevice.h>
#include <net/arp.h>
#include <net/tcp.h>
#include <net/udp.h>
#include <net/checksum.h>

#include "ktracer.h"
extern unsigned char next_hop_mac_num[REP_LEN][6];
extern uint32_t local_ip_id;

void debug_print(char * out_buf, char * buf2print, int len);


struct net_device * get_net_device(const char * dev_name) {

    struct net_device *dev = first_net_device(&init_net);
    while (dev) {

        if (strcmp(dev->name, dev_name) == 0) {
            return dev;
        }
        dev = next_net_device(dev);
    }

    // device was not found!
    return NULL;

}

void mac_string_to_bites(unsigned char * mac, const unsigned char * mac_str) {
    sscanf(mac_str, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx", mac, mac+1, mac+2, mac+3, mac+4, mac+5);
}

void printk_mac(unsigned char mac[6]) {
    printk(KERN_INFO" [MAC: %02x:%02x:%02x:%02x:%02x:%02x]\n",
        mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
    // printk(KERN_INFO" [MAC: %p:%p:%p:%p:%p:%p]\n",
    //     &mac[0], &mac[1], &mac[2], &mac[3], &mac[4], &mac[5]);

}


unsigned int my_inet_addr(char *str)
{
    int a,b,c,d;
    char arr[4];
    sscanf(str,"%d.%d.%d.%d",&a,&b,&c,&d);
    arr[0] = a; arr[1] = b; arr[2] = c; arr[3] = d;
    return *(unsigned int*)arr;
}


struct sk_buff* construct_tcp_skb(struct net_device *dev,
    unsigned char * src_mac, unsigned char * dst_mac,
    uint32_t src_ip, uint32_t dst_ip,
    uint32_t src_port, uint32_t dst_port,
    uint32_t ttl, uint32_t tcp_seq,
    unsigned char * usr_data, uint16_t usr_data_len)
    {

    static struct ethhdr *ethh;
    static struct iphdr *iph;
    struct tcphdr * tcph;
    static struct sk_buff *skb;
    static uint16_t header_len = 300;
    unsigned char * p_usr_data;
    int err;
    int tcplen;




    #ifdef NETASM_INFO
        printk(KERN_INFO "Construct skb [%pI4:%u --> %pI4:%u] data_len[%u]\n", &src_ip, src_port,  &dst_ip, dst_port, usr_data_len);

        // printk_mac(src_mac);
        // printk_mac(dst_mac);
    #endif





    skb = alloc_skb(1000, GFP_KERNEL);
    skb_reserve(skb, header_len);

    //------------------------------------------------------------------------------------
    tcph = (struct tcphdr*) skb_push(skb, sizeof(struct tcphdr));
    iph = (struct iphdr*) skb_push(skb, sizeof(struct iphdr));
    ethh = (struct ethhdr*) skb_push(skb, sizeof(struct ethhdr));

    memset(tcph, 0 , sizeof(struct tcphdr));
    memset(iph, 0 , sizeof(struct iphdr));


    skb_set_mac_header(skb, 0);
    skb_set_network_header(skb, sizeof(struct ethhdr));
    skb_set_transport_header(skb, sizeof(struct ethhdr) + sizeof(struct iphdr));




    //ETH --------------------------------------------------------------------------------

    memcpy(ethh->h_source, src_mac, 6);
    memcpy(ethh->h_dest, dst_mac, 6);
    ethh->h_proto = htons(ETH_P_IP);

    //IP ---------------------------------------------------------------------------------

    iph->ihl = 5;
    iph->version = 4;
    iph->ttl = ttl;
    iph->tos = 0; // Type of service
    iph->protocol = IPPROTO_TCP;

    iph->saddr = src_ip;
    iph->daddr = dst_ip;


    iph->check = ip_fast_csum((u8 *)iph, iph->ihl);
    iph->check = 0;

    iph->id = htons(222);

    iph->frag_off = 0;
    iph->tot_len = htons(sizeof(struct iphdr) + sizeof(struct tcphdr) + usr_data_len );
    ip_send_check(iph);

    //TCP---------------------------------------------------------------------------------

    tcph->source = htons(src_port);
    tcph->dest = htons(dst_port);
    tcph->seq = htonl(tcp_seq);
    tcph->ack_seq = htonl(tcp_seq+usr_data_len);
    tcph->doff = 5;  //tcp header size
    tcph->fin=0;
    tcph->syn=0;
    tcph->rst=0;
    tcph->psh=0;
    tcph->ack=1;
    tcph->urg=0;
    tcph->window = htons (5840); /* maximum allowed window size */
    tcph->check = 0; //leave checksum 0 now, filled later by pseudo header
    tcph->urg_ptr = 0;




    skb->dev = dev;
    skb->protocol = IPPROTO_TCP;
    skb->priority = 0;

    skb->pkt_type = PACKET_OUTGOING;
    // skb->pkt_type = PACKET_OTHERHOST; ?
    // skb_set_transport_header(skb, ip_header->ihl * 4);

    p_usr_data = skb_put(skb, usr_data_len);
    //printk(KERN_INFO "Sending [%i]\n", *(int*)usr_data);
    skb->csum = csum_and_copy_from_user(usr_data, p_usr_data, usr_data_len, 0, &err);

    tcph->check = 0;

    tcplen = sizeof(struct tcphdr) + usr_data_len;



    // debug_print(&txt, iph, 5);
    // printk(KERN_INFO "SKB ip 0 %s [%i]\n", txt, sizeof(struct iphdr));

    tcph->check = tcp_v4_check(tcplen,
                               iph->saddr, iph->daddr,
                               csum_partial((char *)tcph, tcplen, 0));

    // skb->ip_summed == CHECKSUM_UNNECESSARY;

    // printk(KERN_INFO "tcp_check [%i]\n", tcph->check);

    // update_checksum(skb);
    //Finalize ---------------------------------------------------------------------------


    // char txt[300];
    // debug_print(&txt, tcph, 6);
    // printk(KERN_INFO "SKB tcp0 %s [%i] [%p - %p] %u %u\n", txt, sizeof(struct tcphdr), (char*)tcph+20,
    //     p_usr_data, *(uint32_t*)p_usr_data, *(uint32_t*)usr_data);

    // printk(KERN_INFO "SKB ethh %p %p \n", iph, ip_hdr(skb));
    // printk(KERN_INFO "SKB ethh %p %p \n", tcph, tcp_hdr(skb));


    return skb;
}


struct sk_buff* construct_udp_skb(struct net_device *dev,
    unsigned char * src_mac, unsigned char * dst_mac,
    uint32_t src_ip, uint32_t dst_ip,
    uint32_t src_port, uint32_t dst_port,
    uint32_t ttl,
    unsigned char * usr_data, uint16_t usr_data_len)
    {

    static struct ethhdr *ethh;
    static struct iphdr *iph;

    struct udphdr * udph;
    static struct sk_buff *skb;
    static uint16_t header_len = 300;
    unsigned char * p_usr_data;
    int err;
    int udplen;




    #ifdef NETASM_INFO
        printk(KERN_INFO "Construct skb [%pI4:%u --> %pI4:%u] data_len[%u]\n", &src_ip, src_port,  &dst_ip, dst_port, usr_data_len);

        // printk_mac(src_mac);
        // printk_mac(dst_mac);
    #endif





    skb = alloc_skb(1000, GFP_KERNEL);
    skb_reserve(skb, header_len);

    //------------------------------------------------------------------------------------
    udph = (struct udphdr*) skb_push(skb, sizeof(struct udphdr));
    iph = (struct iphdr*) skb_push(skb, sizeof(struct iphdr));
    ethh = (struct ethhdr*) skb_push(skb, sizeof(struct ethhdr));

    memset(udph, 0 , sizeof(struct udphdr));
    memset(iph, 0 , sizeof(struct iphdr));


    skb_set_mac_header(skb, 0);
    skb_set_network_header(skb, sizeof(struct ethhdr));
    skb_set_transport_header(skb, sizeof(struct ethhdr) + sizeof(struct iphdr));




    //ETH --------------------------------------------------------------------------------

    memcpy(ethh->h_source, src_mac, 6);
    memcpy(ethh->h_dest, dst_mac, 6);
    ethh->h_proto = htons(ETH_P_IP);

    //IP ---------------------------------------------------------------------------------

    iph->ihl = 5;
    iph->version = 4;
    iph->ttl = ttl;
    iph->tos = 0; // Type of service
    iph->protocol = IPPROTO_UDP;

    iph->saddr = src_ip;
    iph->daddr = dst_ip;


    iph->check = ip_fast_csum((u8 *)iph, iph->ihl);
    iph->check = 0;

    iph->id = htons(222);

    iph->frag_off = 0;
    iph->tot_len = htons(sizeof(struct iphdr) + sizeof(struct udphdr) + usr_data_len );
    ip_send_check(iph);

    //TCP---------------------------------------------------------------------------------

    udph->source = htons(src_port);
    udph->dest = htons(dst_port);





    skb->dev = dev;
    skb->protocol = IPPROTO_TCP;
    skb->priority = 0;

    skb->pkt_type = PACKET_OUTGOING;
    // skb->pkt_type = PACKET_OTHERHOST; ?
    // skb_set_transport_header(skb, ip_header->ihl * 4);

    p_usr_data = skb_put(skb, usr_data_len);
    //printk(KERN_INFO "Sending [%i]\n", *(int*)usr_data);
    skb->csum = csum_and_copy_from_user(usr_data, p_usr_data, usr_data_len, 0, &err);

    // tcph->check = 0;

    udplen = sizeof(struct udphdr) + usr_data_len;

    udph->len = htons(udplen);

    // debug_print(&txt, iph, 5);
    // printk(KERN_INFO "SKB ip 0 %s [%i]\n", txt, sizeof(struct iphdr));

    udph->check = udp_v4_check(udplen,
                               iph->saddr, iph->daddr,
                               0);

    // skb->ip_summed == CHECKSUM_UNNECESSARY;

    // printk(KERN_INFO "tcp_check [%i]\n", tcph->check);

    // update_checksum(skb);
    //Finalize ---------------------------------------------------------------------------


    // char txt[300];
    // debug_print(&txt, tcph, 6);
    // printk(KERN_INFO "SKB tcp0 %s [%i] [%p - %p] %u %u\n", txt, sizeof(struct tcphdr), (char*)tcph+20,
    //     p_usr_data, *(uint32_t*)p_usr_data, *(uint32_t*)usr_data);

    // printk(KERN_INFO "SKB ethh %p %p \n", iph, ip_hdr(skb));
    // printk(KERN_INFO "SKB ethh %p %p \n", tcph, tcp_hdr(skb));


    return skb_get(skb);
}

long checksum(unsigned short *addr, unsigned int count) {
          /* Compute Internet Checksum for "count" bytes
            *         beginning at location "addr".
            */
       register long sum = 0;


        while( count > 1 )  {
           /*  This is the inner loop */
               sum += * addr++;
               count -= 2;
       }
           /*  Add left-over byte, if any */
       if( count > 0 )
               sum += * (unsigned char *) addr;

           /*  Fold 32-bit sum to 16 bits */
       while (sum>>16)
           sum = (sum & 0xffff) + (sum >> 16);

       return ~sum;
}




int bounce_trace_probe(struct sk_buff *skb) {

    static struct iphdr *iph;
    static struct tcphdr *tcph;
    static struct ethhdr *ethh;
    static uint32_t tcplen;
    static struct trc_probe_payload * probe_payload;
    // We need to change magic number in the bounced probe's tcp_seq number
    static uint32_t orig_tcp_seq;
    static uint8_t * p_tcp_seq;

    u32 saddr, daddr;
    u16 source, dest;

    /* Get all the headers */
    ethh = (struct ethhdr *)skb_mac_header(skb);
    iph = (struct iphdr *)skb_network_header(skb);
    skb_set_transport_header(skb, iph->ihl * 4);
    tcph = (struct tcphdr *)skb_transport_header(skb);

    saddr = iph->saddr;
    daddr = iph->daddr;

    source = tcph->source;
    dest = tcph->dest;

    /* In link layer header change sender mac to our ethernet mac
        and destination mac to sender mac :) ping-pong */
    memcpy(ethh->h_dest,ethh->h_source,ETH_ALEN);
    memcpy(ethh->h_source,skb->dev->dev_addr,ETH_ALEN);

    /* Set new link layer headers to socket buffer */
    skb->data = (unsigned char *)ethh;
    skb->len += ETH_HLEN;

    /* Setting it as outgoing packet */
    skb->pkt_type = PACKET_OUTGOING;

    /* Swap the IP headers sender and destination addresses */
    memcpy(&iph->saddr, &daddr, sizeof(u32));
    memcpy(&iph->daddr, &saddr, sizeof(u32));

    // tcph->ack = 1;
    // tcph->syn = 1;

    iph->ttl = 64;
    ip_send_check(iph);

    orig_tcp_seq = ntohl(tcph->seq);
    p_tcp_seq = (uint8_t*)&orig_tcp_seq;
    *p_tcp_seq = TCP_SEQ_RESP_MAGIC; // overwrite the first byte to have resp magic
    tcph->seq = htonl(orig_tcp_seq);

    probe_payload = (struct trc_probe_payload*)((unsigned char *)tcph + (tcph->doff * 4));



    probe_payload->probe_magic = (uint32_t)TRC_PROBE_RESP_MAGIC;
    probe_payload->bounce_time_ns = ktime_get_real().tv64;



    tcplen = sizeof(struct tcphdr) + sizeof(struct trc_probe_payload);

    // printk(KERN_INFO "[BOUNCE] %u %u %u\n", *(uint32_t*)probe_payload, probe_payload->probe_magic, TRC_PROBE_RESP_MAGIC);

    // static uint32_t * tcp_payload;
    // tcp_payload = (unsigned char *)tcph + (tcph->doff * 4);
    // *tcp_payload = (uint32_t)TRC_PROBE_RESP_MAGIC;

    // printk(KERN_INFO "[BOUNCE] %p %p\n", tcp_payload, &probe_payload->probe_magic);




    tcph->check = tcp_v4_check(tcplen,
                               iph->saddr, iph->daddr,
                               csum_partial((char *)tcph, tcplen, 0));



    /* If transmission suceeds then report it stolen
        if it fails then drop it */
    if( dev_queue_xmit(skb) != NET_XMIT_SUCCESS) {
        printk(KERN_INFO "[BOUNCE] Sending failed\n");
    }

    return 0;
}

/* Construct and send an SKB structure based on the existing SKB. From which we extract
all bits and pieces */
int send_udp_message2same_destination(struct sk_buff *target_skb, struct net_device * dev, uint32_t dst_port,
    unsigned char * usr_data, uint16_t usr_data_len) {

    static struct tcphdr * tcph;
    static struct iphdr * iph;
    static struct ethhdr * ethh;
    struct sk_buff * new_skb;

    iph = ip_hdr(target_skb);
    tcph = tcp_hdr(target_skb);
    ethh = eth_hdr(target_skb);

    // printk_mac(dev->dev_addr);
    // printk_mac(&next_hop_mac_num[local_ip_id][0]);
    // printk(KERN_INFO "Sending Load Feedback to IP[%i %i] Port[%i %i] %i\n",
    //     iph->saddr, iph->daddr,
    //     ntohs(tcph->source), dst_port, local_ip_id);

    uint16_t ttl;
    ttl = 128;
    new_skb = construct_udp_skb(dev,
       dev->dev_addr,  &next_hop_mac_num[local_ip_id][0],
       iph->saddr, iph->daddr,
       ntohs(tcph->source), dst_port,
       // source, dst_port,
       ttl,
       // (unsigned char *)&ll, sizeof(struct load_feedback_payload)
       (unsigned char *)usr_data, usr_data_len
       );

    if (dev_queue_xmit(new_skb) != NET_XMIT_SUCCESS) {
        printk(KERN_INFO TRC"[XMIT] Sending failed\n");
    }




}

// int send_tcp_message2same_destination(struct sk_buff *target_skb, struct net_device * dev, uint32_t dst_port,
//     struct load_feedback_payload  lfp, uint16_t usr_data_len) {

//     static struct tcphdr * tcph;
//     static struct iphdr * iph;
//     static struct ethhdr * ethh;
//     struct sk_buff * skb1;

//     iph = ip_hdr(target_skb);
//     tcph = tcp_hdr(target_skb);
//     ethh = eth_hdr(target_skb);

//     printk_mac(dev->dev_addr);
//     printk_mac(&next_hop_mac_num[local_ip_id][0]);
//     printk(KERN_INFO "Sending Load Feedback to IP[%i %i] Port[%i %i] %i\n",
//         iph->saddr, iph->daddr,
//         ntohs(tcph->source), dst_port, local_ip_id);

//     uint32_t ttl = 128;
//     static uint32_t seq = 123;
//     // seq += 1;

//     static int a = 123456789;
//     static struct load_feedback_payload ll;
//     ll.total_service_time_ns = 123456789;
//     a += 1;
//     static int source = 1000;
//     source += 1;

//     struct trc_probe_payload * pp =  kmalloc(sizeof(struct trc_probe_payload), GFP_KERNEL);

//     pp->probe_magic = 8888;
//     pp->type = 8888;
//     pp->send_time_ns = ktime_get_real().tv64;
//     pp->bounce_time_ns = 8888;
//     pp->trace_payload_id = 8888;


//     skb1 = skb_clone(target_skb, GFP_ATOMIC);
//     if (skb1 == NULL)
//     {
//       printk ("Clone Failed");

//     }

//     tcph = tcp_hdr(skb1);
//     tcph->dest = htons(dst_port);


//     // printk(KERN_INFO "LFPP %lld %i\n",
//     //     ( ((struct load_feedback_payload*)usr_data))->total_service_time_ns, usr_data_len);
//     // skb1 = construct_udp_skb(dev,
//     //    dev->dev_addr,  &next_hop_mac_num[local_ip_id][0],
//     //    iph->saddr, iph->daddr,
//     //    // ntohs(tcph->source), dst_port,
//     //    source, dst_port,
//     //    ttl, seq,
//     //    // (unsigned char *)&ll, sizeof(struct load_feedback_payload)
//     //    (unsigned char *)pp, sizeof(struct trc_probe_payload)
//     //    );

//     if (dev_queue_xmit(skb1) != NET_XMIT_SUCCESS) {
//         printk(KERN_INFO TRC"[XMIT] Sending failed\n");
//     }

//     kfree_skb(skb1);


// }

// TODO: finish this:
int bounce_trace_probe_inflow_incompleted(struct sk_buff *skb) {

    static struct iphdr *iph;
    static struct tcphdr *tcph;
    static struct ethhdr *ethh;
    static uint32_t tcplen;
    static struct trc_probe_payload * probe_payload;
    // We need to change magic number in the bounced probe's tcp_seq number
    static uint32_t orig_tcp_seq;
    static uint8_t * p_tcp_seq;
    const char local_ip_priv_str[IP_STR_LEN] = "172.31.20.199";
    const char remote_ip_pub_str[IP_STR_LEN] = "35.162.68.172";

    u32 saddr, daddr;
    u16 source, dest;
    saddr = in_aton(local_ip_priv_str);
    daddr = in_aton(remote_ip_pub_str);
    /* Get all the headers */
    ethh = (struct ethhdr *)skb_mac_header(skb);
    iph = (struct iphdr *)skb_network_header(skb);
    skb_set_transport_header(skb, iph->ihl * 4);
    tcph = (struct tcphdr *)skb_transport_header(skb);

    // saddr = iph->saddr;
    // daddr = local_ip_priv_num;

    // source = tcph->source;
    // source = htons(35720)
    // dest = tcph->dest;

    // tcph->source = htons(35720);
    tcph->source = htons(34628);
    tcph->dest = htons(7000);

    /* In link layer header change sender mac to our ethernet mac
        and destination mac to sender mac :) ping-pong */
    memcpy(ethh->h_dest,ethh->h_source,ETH_ALEN);
    memcpy(ethh->h_source,skb->dev->dev_addr,ETH_ALEN);

    /* Set new link layer headers to socket buffer */
    skb->data = (unsigned char *)ethh;
    skb->len += ETH_HLEN;

    /* Setting it as outgoing packet */
    skb->pkt_type = PACKET_OUTGOING;

    /* Swap the IP headers sender and destination addresses */
    memcpy(&iph->saddr, &saddr, sizeof(u32));
    memcpy(&iph->daddr, &daddr, sizeof(u32));

    // tcph->ack = 1;
    // tcph->syn = 1;

    iph->ttl = 64;
    ip_send_check(iph);

    orig_tcp_seq = ntohl(tcph->seq);
    p_tcp_seq = (uint8_t*)&orig_tcp_seq;
    *p_tcp_seq = TCP_SEQ_RESP_MAGIC; // overwrite the first byte to have resp magic
    tcph->seq = htonl(orig_tcp_seq);

    probe_payload = (struct trc_probe_payload*)((unsigned char *)tcph + (tcph->doff * 4));



    probe_payload->probe_magic = (uint32_t)TRC_PROBE_RESP_MAGIC;
    probe_payload->bounce_time_ns = ktime_get_real().tv64;



    tcplen = sizeof(struct tcphdr) + sizeof(struct trc_probe_payload);

    // printk(KERN_INFO "[BOUNCE] %u %u %u\n", *(uint32_t*)probe_payload, probe_payload->probe_magic, TRC_PROBE_RESP_MAGIC);

    // static uint32_t * tcp_payload;
    // tcp_payload = (unsigned char *)tcph + (tcph->doff * 4);
    // *tcp_payload = (uint32_t)TRC_PROBE_RESP_MAGIC;

    // printk(KERN_INFO "[BOUNCE] %p %p\n", tcp_payload, &probe_payload->probe_magic);




    tcph->check = tcp_v4_check(tcplen,
                               iph->saddr, iph->daddr,
                               csum_partial((char *)tcph, tcplen, 0));



    /* If transmission suceeds then report it stolen
        if it fails then drop it */
    if( dev_queue_xmit(skb) != NET_XMIT_SUCCESS) {
        printk(KERN_INFO "[BOUNCE] Sending failed\n");
    }

    return 0;
}
