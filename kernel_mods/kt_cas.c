// A set of Cassandra specific functions to parse its header etc.
#include <linux/kernel.h>
#include <linux/tcp.h>
#include <linux/ip.h>

#define CAS_MSG_MSG_ID_POSITION_BYTE 4
#define CAS_MSG_VERB_POSITION_BYTE 17

// These are the Endians converted values, avoid unnecessary conversion at a run time
// See other Cassandra's verbs in MessagingService.java
// This is 3
#define CAS_VERB_READ_REQUEST 50331648
// This is 4
#define CAS_VERB_REQUEST_RESPONSE 67108864


void debug_print(char * out_buf, char * buf2print, int len) {
    int i;
    int offset = 0;
    for (i = 0; i< len; i+=1) {
        offset += sprintf(out_buf+offset, "0x%08x ", *(int*)(buf2print+i*4));
    }
}


inline void parse_cassandra_hdrinfo(struct sk_buff *skb, uint32_t * msg_id, uint32_t * verb) {
    static char * payload;

    payload = (char *)((unsigned char *)tcp_hdr(skb) + (tcp_hdr(skb)->doff * 4));

    // char txt[100];
    // debug_print(&txt, payload, 8);
    // printk(KERN_INFO "cas_hdr %s\n", txt);

    *msg_id = *(uint32_t*)(payload + CAS_MSG_MSG_ID_POSITION_BYTE);
    *verb = *(uint32_t*)(payload + CAS_MSG_VERB_POSITION_BYTE);
}


/* This is application specific, defines different points of when request has been
timestamped. For example, we timestamp at the kernel and user. If verb is read request
and we receiving this packet, then order == 3, because previous 2 timestamped occurred
at the sender (first) in user space and (second) on sending out in kernel */
inline uint32_t get_order_id_incoming(uint32_t verb){

    if (verb == CAS_VERB_READ_REQUEST) {
        return 3;
    } else if (verb == CAS_VERB_REQUEST_RESPONSE) {
        return 7;
    }

    return 0;
}

inline uint32_t get_order_id_outgoing(uint32_t verb){

    if (verb == CAS_VERB_READ_REQUEST) {
        return 2;
    } else if (verb == CAS_VERB_REQUEST_RESPONSE) {
        return 6;
    }
    return 0;
}


/* A single request ID is not enough to differentiate among requests and responses
among multiple replicas, as request id is just a monotonically increasing number.
This we need to add IP addresses of the request originator into the picture.
Thus when machine A sends a request it also logs it's IP, when machine B receives the
request, it logs, the SRC ip (of machine A) along with the msg id (from machine A)
Thus we can match them all alter on */
inline uint32_t get_reference_ip_incoming(uint32_t verb, struct sk_buff *skb){
    static struct iphdr *ip_header;

    ip_header = (struct iphdr *)skb_network_header(skb);

    // if (!ip_header) {
    //     printk(KERN_INFO "get_reference_ip_incoming ERROR\n");
    // }

    if (verb == CAS_VERB_READ_REQUEST) {
        // If we are reading request, then the src IP is the reference point
        return (uint32_t)ip_header->saddr;

    } else {  // verb == CAS_VERB_REQUEST_RESPONSE
        // If we reading a response then "our" ip is the reference
        return  (uint32_t)ip_header->daddr;
    }
}

inline uint32_t get_reference_ip_outgoing(uint32_t verb, struct sk_buff *skb){
    static struct iphdr *ip_header;

    ip_header = (struct iphdr *)skb_network_header(skb);

    // if (!ip_header) {
    //     printk(KERN_INFO "get_reference_ip_outgoing ERROR\n");
    // }

    if (verb == CAS_VERB_READ_REQUEST) {
        // If we are writing request, then "our" ip is the reference
        return  (uint32_t)ip_header->saddr;

    } else {  // verb == CAS_VERB_REQUEST_RESPONSE
        // If we writing a response then the destination ip is the reference
        return (uint32_t)ip_header->daddr;
    }
}

inline int is_cas_read_request(uint32_t verb) {
    return (verb == CAS_VERB_READ_REQUEST);
}

inline int is_cas_read_response(uint32_t verb) {
    return (verb == CAS_VERB_REQUEST_RESPONSE);
}
