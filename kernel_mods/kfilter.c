#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/netfilter.h>
#include <linux/netdevice.h>
#include <linux/netfilter_ipv4.h>
#include <linux/if_ether.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>

#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/udp.h>
#include <uapi/linux/icmp.h>
#include <linux/icmp.h>


#include "ktproc.h"
#include "ktracer.h" /* to get FLTR2USR_COM_VIA_NETLINK/PROC defines */


extern struct load_feedback_payload;
/* ktracer_nlink.c **********************************************************************/
struct sock * init_netlink_socket(void);


/* ktracer_probe.c **********************************************************************/
uint32_t unpack_tcp_seq_magic(uint32_t seq_number);
int is_incoming_trc_rqst(uint32_t tcph_seq, uint32_t * tcp_payload);
int is_incoming_trc_resp(uint32_t tcph_seq, uint32_t * tcp_payload);

static struct nf_hook_ops nfin;
static struct nf_hook_ops nfout;

int setup_proc_incoming_log(void);
int setup_proc_outgoing_log(void);
int setup_proc_ktracer_bounce_log(void);
int setup_proc_ktracer_recv(void);
int setup_proc_ktracer_loadfeedback(void);

void remove_proc_incoming_log(void);
void remove_proc_outgoing_log(void);
void remove_proc_ktracer_bounce(void);
void remove_proc_ktracer_recv(void);
void remove_proc_ktracer_loadfeedback(void);

/* ktproc.c *****************************************************************************/
int log2proc_incoming_packet(uint32_t tstmp_order, uint32_t msg_id, uint32_t verb_opcode,
    ktime_t tstamp, uint32_t reference_ip, uint16_t src_port);
int log2proc_outgoing_packet(uint32_t tstmp_order, uint32_t msg_id, uint32_t verb_opcode,
    ktime_t tstamp, uint32_t reference_ip, uint16_t dst_port);
int log2proc_returning_icmp_ttl_exceeded(struct trc_icmpte_usr_report * uspace_msg);
int log2proc_bounced_probe_exceeded(struct trc_bounce_usr_report * uspace_msg);
void log2proc_load_feedback(struct load_feedback_payload * lfb, uint32_t src_ip);

/* kt_cas.c *****************************************************************************/
void debug_print(char * out_buf, char * buf2print, int len);
void parse_cassandra_hdrinfo(struct sk_buff *skb, uint32_t * msg_id, uint32_t * verb);
uint32_t get_order_id_incoming(uint32_t verb);
uint32_t get_order_id_outgoing(uint32_t verb);

uint32_t get_reference_ip_incoming(uint32_t verb, struct sk_buff *skb);
uint32_t get_reference_ip_outgoing(uint32_t verb, struct sk_buff *skb);
inline int is_cas_read_request(uint32_t verb);
inline int is_cas_read_response(uint32_t verb);

/* network_common.c *********************************************************************/
int bounce_trace_probe(struct sk_buff *skb);
struct net_device * get_net_device(const char * dev_name);

/* ktec_tracer.c ************************************************************************/
void assemble_fltr2usr_resp_report(
    struct trc_bounce_usr_report * uspace_msg, struct sk_buff *skb);
void assemble_fltr2usr_icmp_report(
    struct trc_icmpte_usr_report * uspace_msg, struct sk_buff *skb);

/* netlink.c ************************************************************************/
int send2usr_via_netlink(
    struct sock * s, uint32_t net_link_group, unsigned char * data, int data_len);

/* krcv_rsp_matcher.c *******************************************************************/
void matcher_init_hash_tables(void);
void matcher_free_hash_tables(void);
void matcher_init_FIFO_queue(void);
void matcher_free_FIFO_queue(void);

void hash_sample(void);
void matcher_add_request2hash(uint32_t request_id, uint64_t tstamp_ns, uint32_t src_ip);
int matcher_match_response_from_hash(uint32_t request_id, uint32_t src_ip);
void matcher_garbage_collect_hash_table(void);
void send_loadfeedback(struct sk_buff *skb, struct net_device * dev);

/* ktracer_ip.c *************************************************************************/
void prepare_ip_lists(void);

/* kt_ycsb.c ****************************************************************************/
int ycsb_is_known_operation(uint8_t opcode);
uint32_t ycsb_get_order_id4incoming_packet(uint16_t opcode);
uint32_t ycsb_get_order_id4outgoing_packet(uint16_t opcode);
uint8_t ycsb_get_version_from_payload(uint8_t * tcp_payload);
uint16_t ycsb_get_streamid_from_payload(uint8_t * tcp_payload);
uint8_t ycsb_get_opcode_from_payload(uint8_t * tcp_payload);

/* Local Functions.c ********************************************************************/
int is_trace_probe(struct sk_buff *skb);

/* VARIABLES ****************************************************************************/
struct sock * nlink_recvr_sock = NULL;

struct net_device *dev;



#ifdef STATS

    // Total number of incoming and outgoing messages
    static uint32_t in_msg_count = 0;
    static uint32_t out_msg_count = 0;

    // Total number of messages directed to/from our target application
    static uint32_t accpt_in_msg_count = 0;
    static uint32_t accpt_out_msg_count = 0;

    // Total number of application specific messages that we could parse
    // i.e., non fragmented packets
    static uint32_t logged_accpt_in_msg_count = 0;
    static uint32_t logged_accpt_out_msg_count = 0;

    // Total number of bytes exchanged by our application
    static uint64_t accept_in_bytes_total = 0;
    static uint64_t accept_out_bytes_total = 0;


#endif

#define INPACK_IGNORE 0
#define INPACK_APP 1            /* Incoming Application packet */
#define INPACK_TRACE_RQST 2     /* Incoming Tectonic Trace from remote */
#define INPACK_TRACE_RESP 3     /* Incoming Response from remote tectonic on our trace */
#define INPACK_ICMP_TTL_EX 4    /* Incoming ICMP Time Exceeded, that was sent from us */
#define INPACK_LOAD_FEEDBACK 5  /* Req.Comp.Time. Dist. Samples from remote node */
#define INPACK_YCSB_REQUEST 6   /* YCSB incoming request */
#define OUTPACK_YCSB_RESPONSE 7 /* Reply to YCSB */

int is_application_packet(struct sk_buff *skb) {

    if ( (skb) &&
         (ip_hdr(skb)->protocol == IPPROTO_TCP) && //port 22555 is 7000 without htons()
         (((unsigned short int) tcp_hdr(skb)->dest) == 22555))  {
        return 1;
    }

    return 0;
}

/** Has to be SUPER fast **/
int classify_packet(struct sk_buff *skb) {

    static struct tcphdr * tcph;
    static struct udphdr * udph;
    static struct iphdr * iph;
    static struct icmphdr * icmph;
    static uint8_t * tcp_payload;
    static struct trc_probe_payload * probe_payload;
    static struct ycsb_msg_payload * ycsb_payload;

    if (skb) {
        iph = ip_hdr(skb);

        if (iph->protocol == IPPROTO_TCP) {
            tcph = tcp_hdr(skb);
            tcp_payload = (uint8_t*)((unsigned char *)tcph + (tcph->doff * 4));

            /* port 22555 is 7000 without htons() */
            if (((unsigned short int) tcp_hdr(skb)->dest) == 22555) {

                probe_payload = (struct trc_probe_payload *)tcp_payload;

                // printk(KERN_INFO "TCP SEQ MAG %i %u %u\n", unpack_tcp_seq_magic(ntohl(tcph->seq)), *(uint32_t*)tcp_payload, probe_payload->probe_magic);
                // printk(KERN_INFO "TCP SEQ MAG %i %p %p\n",
                    // unpack_tcp_seq_magic(ntohl(tcph->seq)), tcp_payload, &probe_payload->probe_magic);

                    // static char * payload;

                    // uint32_t a =  tcph->seq;

                    // struct trc_probe_payload * pp;
                    // pp = (struct trc_probe_payload *)payload;
                    // uint32_t * pv = payload;

                    // int tcp_payload_len = ntohs(iph->tot_len) - (sizeof(struct iphdr) + (tcph->doff * 4));

                    // printk(KERN_INFO "PROBE PP [len:%i] %u %u %u %u\n", tcp_payload_len,
                    //     pp->probe_magic, pp->type, pp->send_time_ns, pp->rcv_time_ns);

                    // char txt[200];
                    // debug_print(&txt, tcph, 10);
                    // printk(KERN_INFO "TCP: %s\n", txt);

                    // printk(KERN_INFO "PROBE PP  %u\n", *pv);

                if (is_incoming_trc_rqst(tcph->seq, tcp_payload)) {
                    return INPACK_TRACE_RQST;
                } else if (is_incoming_trc_resp(tcph->seq, tcp_payload)) {
                    return INPACK_TRACE_RESP;
                } else {
                    return INPACK_APP;
                }
            } else /* port 21027 is 9042 without htons() for YCSB incoming request*/
            if (((unsigned short int) tcp_hdr(skb)->dest) == 21027) {

                if ((ycsb_get_version_from_payload(tcp_payload) == YCSB_V4_REQUEST) &&
                    (ycsb_is_known_operation(
                        ycsb_get_opcode_from_payload(tcp_payload)))
                    ) {

                    return INPACK_YCSB_REQUEST;
                }

            } else /* port 21027 is 9042 without htons() for YCSB outgoing reply*/
            if (((unsigned short int) tcp_hdr(skb)->source) == 21027) {

                if ((ycsb_get_version_from_payload(tcp_payload) == YCSB_V4_RESPONSE) &&
                    (ycsb_is_known_operation(
                        ycsb_get_opcode_from_payload(tcp_payload)))
                    ) {
                    return OUTPACK_YCSB_RESPONSE;
                }
            }

        } // END TCP Packets
        else if (iph->protocol == IPPROTO_UDP) {

            udph = udp_hdr(skb);
            if (ntohs(tcp_hdr(skb)->dest) == PORT_LOAD_FEEDBACK) {
                return INPACK_LOAD_FEEDBACK;
            }

        } // END UDP Packets
        else if (iph->protocol == IPPROTO_ICMP) {

            icmph = icmp_hdr(skb);

            if ((icmph->type == ICMP_TIME_EXCEEDED) &&
                (icmph->code == ICMP_EXC_TTL)) {

                return INPACK_ICMP_TTL_EX;
            }
        } // END ICMP Packets
    }

    return INPACK_IGNORE;
}

void log_incoming_msgs2klog(struct sk_buff *skb, uint32_t pckt_class) {

    switch (pckt_class) {
        case INPACK_IGNORE:
            break;

        case INPACK_APP:

            break;
        case INPACK_TRACE_RQST:
            printk(KERN_INFO FLTR"Incoming Trace Request\n");
            break;
        case INPACK_TRACE_RESP:
            printk(KERN_INFO FLTR"Incoming Trace Response\n");
            break;
        case INPACK_ICMP_TTL_EX:
            printk(KERN_INFO FLTR"Incoming TTL TE\n");
            break;
    }

}

unsigned int check_incoming_skb(struct sk_buff *skb) {
    static struct tcphdr * tcph;
    static struct udphdr * udph;
    static struct iphdr * iph;

    static uint32_t cas_msg_id;
    static uint32_t cas_verb;
    static uint32_t tstmp_order;
    static uint32_t reference_ip;

    static struct trc_icmpte_usr_report uspace_msg_icmp;
    static struct trc_bounce_usr_report uspace_msg;

    static uint8_t * tcp_payload;
    static uint64_t pckt_class;
    static struct load_feedback_payload * lfb;

    /// YCSB
    uint16_t ycsb_stream_id;
    uint8_t ycsb_opcode;

    pckt_class = classify_packet(skb);

    #ifdef LOG_FLTR_MSG_TYPE
        log_incoming_msgs2klog(skb, pckt_class);
    #endif

    switch (pckt_class) {

        case INPACK_IGNORE:
            return NF_ACCEPT;

        case INPACK_APP:
            #ifdef STATS
                accpt_in_msg_count++;
                accept_in_bytes_total += skb->len;
            #endif

            parse_cassandra_hdrinfo(skb, &cas_msg_id, &cas_verb);

            tstmp_order = get_order_id_incoming(cas_verb);

            if (tstmp_order > 0) {
                // we got some expected values in the payload
                #ifdef LOG_IN

                    reference_ip = get_reference_ip_incoming(cas_verb, skb);

                    log2proc_incoming_packet(tstmp_order, cas_msg_id, cas_verb,
                        skb->tstamp, reference_ip, UNUSED_FIELD);

                    if (is_cas_read_request(cas_verb)) {
                        matcher_add_request2hash(cas_msg_id, skb->tstamp.tv64, reference_ip);
                        matcher_garbage_collect_hash_table();
                    }

                    send_loadfeedback(skb, dev);

                    #ifdef STATS
                        logged_accpt_in_msg_count++;
                    #endif
                #else
                    tstmp_order += 1;
                #endif
            }
            return NF_ACCEPT;

        case INPACK_TRACE_RQST:
            /* We have received a traceroute probe from ktracer.
            do work and send it back, do not forward to TCP stack */
            bounce_trace_probe(skb);

            return NF_STOLEN;

        case INPACK_TRACE_RESP:

            // printk(KERN_INFO "Probe Resp!\n");

            assemble_fltr2usr_resp_report(&uspace_msg, skb);
            #ifdef FLTR2USR_COM_VIA_NETLINK
                send2usr_via_netlink(nlink_recvr_sock, NETLINK_KFILTER_GROUP,
                    (unsigned char *)&uspace_msg, sizeof(struct trc_bounce_usr_report));
            #endif

            #ifdef FLTR2USR_COM_VIA_PROC
                log2proc_bounced_probe_exceeded(&uspace_msg);
            #endif

            return NF_STOLEN;


        /* ICMP ------------------------------------------------------------------------*/
        case INPACK_ICMP_TTL_EX:


            assemble_fltr2usr_icmp_report(&uspace_msg_icmp, skb);

            #ifdef FLTR2USR_COM_VIA_NETLINK
                send2usr_via_netlink(nlink_recvr_sock, NETLINK_KFILTER_GROUP,
                    (unsigned char *)&uspace_msg_icmp, sizeof(struct trc_icmpte_usr_report));
            #endif

            #ifdef FLTR2USR_COM_VIA_PROC
                log2proc_returning_icmp_ttl_exceeded(&uspace_msg_icmp);
            #endif
            return NF_STOLEN;


        case INPACK_LOAD_FEEDBACK:
            /* We have received load feedback from a remote host, --> print it to proc */
            iph = ip_hdr(skb);
            udph = udp_hdr(skb);

            tcp_payload = (uint8_t*)((unsigned char *)udph + 8);
            lfb = (struct load_feedback_payload *)tcp_payload;

            // char txt[100];
            // debug_print(&txt, tcp_payload, 4);
            // printk(KERN_INFO "lfb %s\n", txt);

            log2proc_load_feedback(lfb, (uint32_t)iph->saddr);

            return NF_STOLEN;

        case INPACK_YCSB_REQUEST:
            #ifdef STATS
                accpt_in_msg_count++;
                accept_in_bytes_total += skb->len;
            #endif

            #ifdef LOG_IN

                iph = ip_hdr(skb);
                tcph = tcp_hdr(skb);

                tcp_payload = (uint8_t *)((unsigned char *)tcph + (tcph->doff * 4));

                ycsb_stream_id = ycsb_get_streamid_from_payload(tcp_payload);
                ycsb_opcode = ycsb_get_opcode_from_payload(tcp_payload);

                tstmp_order = ycsb_get_order_id4incoming_packet(ycsb_opcode);

                log2proc_incoming_packet(tstmp_order,
                    ycsb_stream_id, ycsb_opcode, ktime_get(),
                    (uint32_t)iph->saddr, (uint16_t)tcph->source);

                #ifdef STATS
                    logged_accpt_in_msg_count++;
                #endif
            #endif

            return NF_ACCEPT;
    }

    return NF_ACCEPT;
}

/*--------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------*/
/* Outgoing Packets --------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------*/

void check_outgoing_skb(struct sk_buff *skb) {
    static struct tcphdr * tcph;
    static struct iphdr * iph;

    static uint32_t cas_msg_id;
    static uint32_t cas_verb;
    static uint32_t tstmp_order;
    static uint32_t reference_ip;

    static uint32_t * tcp_payload;
    static uint64_t pckt_class;

    /// YCSB
    uint16_t ycsb_stream_id;
    uint8_t ycsb_opcode;


    pckt_class = classify_packet(skb);

    switch (pckt_class) {

        case OUTPACK_YCSB_RESPONSE:
            #ifdef STATS
                accpt_out_msg_count++;
                accept_out_bytes_total += skb->len;
            #endif

            #ifdef LOG_OUT

                iph = ip_hdr(skb);
                tcph = tcp_hdr(skb);

                tcp_payload = (uint32_t*)((unsigned char *)tcph + (tcph->doff * 4));

                ycsb_stream_id = ycsb_get_streamid_from_payload(tcp_payload);
                ycsb_opcode = ycsb_get_opcode_from_payload(tcp_payload);

                tstmp_order = ycsb_get_order_id4outgoing_packet(ycsb_opcode);

                log2proc_outgoing_packet(tstmp_order,
                    ycsb_stream_id, ycsb_opcode, ktime_get(),
                    (uint32_t)iph->daddr, (uint16_t)tcph->dest);

                #ifdef STATS
                    logged_accpt_out_msg_count++;
                #endif
            #endif

            return NF_ACCEPT;
    }

    if (is_application_packet(skb)) {
        #ifdef STATS
            accpt_out_msg_count++;
            accept_out_bytes_total += skb->len;
        #endif

        parse_cassandra_hdrinfo(skb, &cas_msg_id, &cas_verb);

        tstmp_order = get_order_id_outgoing(cas_verb);

        if (tstmp_order > 0) {
            // we got some expected values in the payload
            #ifdef LOG_OUT

                reference_ip = get_reference_ip_outgoing(cas_verb, skb);

                log2proc_outgoing_packet(tstmp_order, cas_msg_id, cas_verb,
                    ktime_get_real(), reference_ip, UNUSED_FIELD);

                if (is_cas_read_response(cas_verb)) {
                    matcher_match_response_from_hash(cas_msg_id, reference_ip);
                }

                #ifdef STATS
                    logged_accpt_out_msg_count++;
                #endif
            #else
                // leaving here to test perfs. So that compiler wont optimize out IFs,
                tstmp_order += 1;
            #endif
        }
    }
}



static unsigned int hook_func_in(const struct nf_hook_ops *ops,
        struct sk_buff *skb, const struct net_device *in,
        const struct net_device *out, int (*okfn)(struct sk_buff *) ) {

    unsigned int retval = check_incoming_skb(skb);


    #ifdef STATS
        in_msg_count++;
    #endif

    return retval;
}



static unsigned int hook_func_out(const struct nf_hook_ops *ops,
        struct sk_buff *skb, const struct net_device *in,
        const struct net_device *out, int (*okfn)(struct sk_buff *)) {

    check_outgoing_skb(skb);

    #ifdef STATS
        out_msg_count++;
    #endif

    return NF_ACCEPT;
}





static int __init init_main(void) {
    /**
    Note: the order of operation on init and exit are important. If logging is performed
    then proc buffers should be initialized _before_ we attempts to store timestamps in them
    **/

    #ifdef LOG_IN
        setup_proc_incoming_log();
    #endif

    #ifdef FLTR2USR_COM_VIA_PROC
        setup_proc_ktracer_bounce_log();
        setup_proc_ktracer_recv();
        setup_proc_ktracer_loadfeedback();
    #endif

    #ifdef FLTR2USR_COM_VIA_NETLINK
        nlink_recvr_sock =  init_netlink_socket();
        if (!nlink_recvr_sock) {
            return -1;
        }
    #endif

    #ifdef SAMPLE_IN
        nfin.hook     = hook_func_in;
        nfin.hooknum  = NF_INET_PRE_ROUTING;
        nfin.pf       = PF_INET;
        nfin.priority = NF_IP_PRI_FIRST;
        nf_register_hook(&nfin);
        printk(KERN_INFO "Tectonic: Loading Prerouting hook\n");
    #endif



    dev = get_net_device(NET_DEV_NAME);
    if (!dev) {
        printk(KERN_INFO "KTimer Cannot find net device\n");
    }

    prepare_ip_lists();

    matcher_init_hash_tables();
    matcher_init_FIFO_queue();


    #ifdef LOG_OUT
        setup_proc_outgoing_log();
    #endif

    #ifdef SAMPLE_OUT
        nfout.hook     = hook_func_out;
        nfout.hooknum  = NF_INET_POST_ROUTING;
        nfout.pf       = PF_INET;
        nfout.priority = NF_IP_PRI_FIRST;
        nf_register_hook(&nfout);
        printk(KERN_INFO "Tectonic: Loading Postrouting hook\n");
    #endif

    #ifdef STATS
        printk(KERN_INFO "KTEC, statistics is collected, report upon exit.\n");
    #endif



    return 0;
}


static void __exit cleanup_main(void) {

    #ifdef SAMPLE_IN
        nf_unregister_hook(&nfin);
    #endif

    #ifdef LOG_IN
        remove_proc_incoming_log();
    #endif

    #ifdef FLTR2USR_COM_VIA_PROC
        remove_proc_ktracer_bounce();
        remove_proc_ktracer_recv();
        remove_proc_ktracer_loadfeedback();
    #endif

    #ifdef FLTR2USR_COM_VIA_NETLINK
        netlink_kernel_release(nlink_recvr_sock);
    #endif


    #ifdef SAMPLE_OUT
        nf_unregister_hook(&nfout);
    #endif

    #ifdef LOG_OUT
        remove_proc_outgoing_log();
    #endif

    matcher_free_hash_tables();
    matcher_init_FIFO_queue();

    #ifdef STATS
        printk(KERN_INFO "Ktec stats\n\
            Total_in/out_msgs:                %5u/%5u\n\
            Total_app_in/out_msgs:            %5u/%5u\n\
            Total_app_logged_in/out_msgs:     %5u/%5u\n\
            Total_app_bytes_in/out            %llu/%llu\n",

            in_msg_count, out_msg_count,
            accpt_in_msg_count, accpt_out_msg_count,
            logged_accpt_in_msg_count, logged_accpt_out_msg_count,
            accept_in_bytes_total, accept_out_bytes_total
            );

    #endif

}
module_init(init_main);
module_exit(cleanup_main);



MODULE_LICENSE("GPL");
