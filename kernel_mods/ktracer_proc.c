#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/slab.h>      // for kmalloc() dynamic allocation


#include "ktproc.h"

#include "ktracer.h"

int setup_proc_file(struct proc_dir_entry * entry, const char * proc_file_name, struct file_operations * file_ops);
int init_con_list(struct text_container *** container_list, int * container_index);
void free_con_list(struct text_container *** container_list);

extern struct seq_operations ct_seq_ops;

static struct  text_container ** ktracer_sender_con_list;
static int ktracer_sender_con_index;
struct proc_dir_entry * proc_entry_ktracer_sender;




int log2proc_ktracer_sender_msg(struct trc_sendr_usr_report * user_msg) {

    static int append_len;
    #ifdef TRC_DEBUG_PROC
        static char debug_msg[100];
    #endif

    struct  text_container * con = ktracer_sender_con_list[ktracer_sender_con_index];


    // If we have enough of space for a message
    if (con->max_size - con->text_size < 50) {


        if (ktracer_sender_con_index + 1 < MAX_BUFFS) {
            ktracer_sender_con_index += 1;
            con = ktracer_sender_con_list[ktracer_sender_con_index];
            // printk(KERN_INFO "Moving to the next container index[%i]\n", *ktracer_sender_con_index);

            if (con->text_size > 0) {
                printk(KERN_ERR "ERROR new container is not empty !\n");
            }


        } else {
            // We used up all our preallocated buffers
            // printk(KERN_INFO "No more containers to dump date\n");
            return -1;
        }

    }

    append_len = sprintf(con->data + con->text_size, "%u %u %u %llu %llu\n",
        user_msg->src_ip,
        user_msg->dst_ip,
        user_msg->trace_tcp_seq_id,
        user_msg->time_ns,
        user_msg->trace_payload_id);

    #ifdef TRC_DEBUG_PROC
        sprintf(debug_msg, "%u %u %u %llu %llu\n",
        user_msg->src_ip,
        user_msg->dst_ip,
        user_msg->trace_tcp_seq_id,
        user_msg->time_ns,
        user_msg->trace_payload_id);
        printk(KERN_INFO "proc: %s\n", debug_msg);
    #endif

    if (append_len < 0) {
        printk(KERN_INFO "Error appending text to the buffer\n");
        return -1;
    } else {
        con->text_size += append_len;
    }

    return 0;

}





//////////////////////////////////////////////////////////////////////////////////////////

static int open_proc_in(struct inode *inode, struct file *file) {

    struct seq_file * sf;
    int res =  seq_open(file, &ct_seq_ops);
    if (res < 0) return res;

    sf = (struct seq_file*)file->private_data;
    sf->private = ktracer_sender_con_list;

    return res;
};



/* These two structures are only differ by the .open action as they need to pass
   individual pointers to their containers */
static struct file_operations file_ktracer_sender_ops = {
    .owner   = THIS_MODULE,
    .open    = open_proc_in,
    .read    = seq_read,
    .llseek  = seq_lseek,
    .release = seq_release
};


int setup_proc_sending_traces(void) {
    int ret;

    ret = init_con_list(&ktracer_sender_con_list, &ktracer_sender_con_index);
    if (ret < 0) return ret;

    ret = setup_proc_file(proc_entry_ktracer_sender, PROC_KTRACER_SENDER_FNAME, &file_ktracer_sender_ops);

    printk(KERN_INFO "Initializing /proc entry for [%s]\n", PROC_KTRACER_SENDER_FNAME);
    return ret;
}



void remove_proc_sending_traces(void) {
    printk(KERN_INFO "Removing /proc entry for [%s]\n", PROC_KTRACER_SENDER_FNAME);

    remove_proc_entry(PROC_KTRACER_SENDER_FNAME, NULL);

    free_con_list(&ktracer_sender_con_list);
}

